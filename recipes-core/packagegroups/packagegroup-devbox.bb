DESCRIPTION = "devbox Package Group"

PACKAGE_ARCH = "${MACHINE_ARCH}"
inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "\
    packagegroup-devbox"

# required to install nix

RDEPENDS:packagegroup-devbox = "\
xz \
"
