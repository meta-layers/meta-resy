# add oprofile, which got lost in jethro
# and remove it again, since deprecated

PROFILETOOLS:append = "\
    bootchart2 \
    ltrace \
    ply \
    trace-cmd \
    dmalloc \
    heaptrack \
    memstat \
    uftrace \
    "
# trace-cmd collides with perf

# sumo removed oprofile
# oprofile

#RRECOMMENDS:${PN}:append = "
#    kernel-module-oprofile
#    "

# --> riscv64
RDEPENDS:${PN}:remove:riscv64 = "ltrace"

RDEPENDS:${PN}:remove:riscv64 = "ply"
# <-- riscv64

