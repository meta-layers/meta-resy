# --> crash
# =================
# crash 8.0.4
# we need a special image with various things built in to make crash 8.0.4 compile on target

# 1) makeinfo
# in order to compile crash on the target we need makeinfo
# makeinfo is in the texinfo pkg
# and texinfo needs Encode.pm from perl-module-encode
# I included lots of perl stuff to make it happy

RRECOMMENDS:packagegroup-core-sdk += "\
    texinfo \
    perl \
    libmodule-build-perl \
    perl-module-encode \
    perl-module-encode-encoding \
    perl-misc \
    perl-modules \
"

# 2) ro_RO.iso88592
#file=`echo ro | sed 's,.*/,,'`.gmo \
#  && rm -f $file && PATH=../src:$PATH /usr/bin/msgfmt -o $file ro.po
#ro.po: warning: Charset "ISO-8859-2" is not supported. msgfmt relies on iconv(),
#                and iconv() does not support "ISO-8859-2".
#                Installing GNU libiconv and then reinstalling GNU gettext
#                would fix this problem.
#                Continuing anyway.
#/usr/bin/msgfmt: Cannot convert from "ISO-8859-2" to "UTF-8". msgfmt relies on iconv(), and iconv() does not support this conversion.
#
# root@multi-v7-ml:~# locale -a
# C
# C.utf8
# POSIX
# de_DE
# de_DE.utf8
# el_GR
# el_GR.utf8
# en_GB
# en_GB.utf8
# en_US
# en_US.utf8
# es_ES
# es_ES.utf8
# fa_IR
# fa_IR.utf8
# fr_FR
# fr_FR.utf8
# hr_HR
# hr_HR.utf8
# ja_JP.eucjp
# lt_LT
# lt_LT.utf8
# pl_PL
# pl_PL.iso88592
# pl_PL.utf8
# ro_RO
# ro_RO.iso88592
# ro_RO.utf8
# ru_RU
# ru_RU.utf8
# tr_TR
# tr_TR.utf8

RRECOMMENDS:packagegroup-core-sdk += "\
    glibc-utils \
    localedef \
    glib-2.0 \ 
    tzdata \
    tzdata-americas \
    tzdata-asia \
    tzdata-europe \
    tzdata-posix \
    glib-2.0-locale-ja \
    glib-2.0-locale-fr \
    glib-2.0-locale-el \
    glib-2.0-locale-hr \
    glib-2.0-locale-lt \
    glib-2.0-locale-pl \
    glib-2.0-locale-ru \
    glib-2.0-locale-ro \
    locale-base-de-de \
    locale-base-es-es \
    locale-base-en-gb \
    locale-base-en-us \
    locale-base-fr-fr \
    locale-base-ru-ru \
    locale-base-ro-ro.iso-8859-2 \
    locale-base-ro-ro \
    glibc-gconv-utf-16 \
    glibc-charmap-utf-8 \
    glibc-gconv-cp1255 \
    glibc-charmap-cp1255 \
    glibc-gconv-utf-32 \
    glibc-gconv-utf-7 \
    glibc-gconv-euc-jp \
    glibc-gconv-iso8859-1 \
    glibc-gconv-iso8859-2 \
    glibc-gconv-iso8859-15 \
    glibc-charmap-invariant \
    glibc-localedata-translit-cjk-variants \
    locale-base-lt-lt \
    locale-base-ja-jp.euc-jp \
    locale-base-fa-ir \
    locale-base-hr-hr \
    locale-base-el-gr \
    locale-base-pl-pl \
    locale-base-pl-pl.iso-8859-2 \
    locale-base-tr-tr \
    locale-base-ro-ro.iso-8859-2 \
    locale-base-ro-ro \
"
# <-- crash
# --> evl
# userspace evl needs libbpf
RRECOMMENDS:packagegroup-core-sdk += "\
    libbpf \
"
# <-- evl
