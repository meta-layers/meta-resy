#
# Copyright (C) 2021 Robert Berger - Reliable Embedded Systems e.U.
# Copyright (C) 2019 SHIMADA Hirofumi - dejiko
# SPDX-License-Identifier: MIT
#

require version_${PV}.inc
require softether-hamcorebuilder-native_git.inc
