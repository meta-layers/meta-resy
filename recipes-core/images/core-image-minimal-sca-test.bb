# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL += "\
                  dropbear \
                  bash \
                  btrfs-tools \
                  udev-extraconf \
                  glibc-gconv-utf-16 \
                  softether-vpnserver \
                  softether-vpnbridge \
                  softether-vpnclient \
                  softether-vpncmd \
                  sca-hello-world \ 
"
