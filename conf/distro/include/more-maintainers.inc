# my stuff e.g. for training
#
# The format is as a bitbake variable override for each recipe
#
#       RECIPE_MAINTAINER:pn-<recipe name> = "Full Name <address@domain>"
#
# Please keep this list in alphabetical order.
#
#RECIPE_MAINTAINER:pn-simple-hello-world = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
#RECIPE_MAINTAINER:pn-simple-hello-world-git = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
