# Note that busybox is required to satify /bin/sh requirement of lghttpd,
# and the access* modules need to be explicitly specified since RECOMMENDATIONS
# are disabled.

# JAVA_RUNTIME is defined in local.conf and is either
# openjdk-8 or openjre-8

IMAGE_INSTALL += " \
	busybox \
	${JAVA_RUNTIME} \
"
