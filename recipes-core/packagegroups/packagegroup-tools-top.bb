DESCRIPTION = "Tools Top Package Group"

inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "\
    packagegroup-tools-top"

RDEPENDS:${PN} = "\
    htop \
    iotop \
    powertop \
    atop \
    iftop \
    ntopng \
    bmon \
    "

# removed monit, since I never use it
# monit

# -->
# this consumes lots of CPU in the beagle bone black
# RDEPENDS:${PN}:remove:beagle-bone-black-conserver = "ntopng"
# RDEPENDS:${PN}:remove:beagle-bone-black-conserver = "monit"
# <--

# sumo removed this
# latencytop

# powerdebug is deprecated
# powerdebug

# honister needs fixing
# tiptop
# iptraf-ng
# nmon
