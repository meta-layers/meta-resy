# SPDX-FileCopyrightText: 2021 Robert Berger <RobertBerger@ReliableEmbeddedSystems.com>
#
# SPDX-License-Identifier: MIT

# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   LICENSE
#   COPYING
#   src/libshared/LICENSE
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=eb1f3393f31ed66374f7d944fdcd188d \
                    file://src/libshared/LICENSE;md5=ef71416c70e6fc223ba9d350e59a2cd0"

SRC_URI = "gitsm://github.com/GothenburgBitFactory/taskwarrior.git;protocol=https;branch=master"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "2f47226f91f0b02f7617912175274d9eed85924f"

S = "${WORKDIR}/git"

DEPENDS = "util-linux-libuuid gnutls"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = ""

