#!/bin/sh
#######################################################################################
# Explanation:
# if we boot with a rootfs over nfs or ip addresses over kernel commandline
# /lib/systemd/network/80-wired.network is not executed
# Like this we loose the local dns server
#
# Solution:
# We can pass ip addresses via the kernel command line in certain ways
#
# e.g.: 
# console=ttymxc1,115200 root=/dev/nfs ip=dhcp nfsroot=192.168.42.107:/opt/poky/imx8mm-lpddr4-evk-ml-rootfs,v3,tcp
# the kernel creates this:
# cat /proc/net/pnp
# #PROTO: DHCP
# domain res.training
# nameserver 192.168.42.254
# bootserver 192.168.42.1
#
# or:
# console=ttymxc1,115200 root=/dev/nfs ip=192.168.42.77:192.168.42.107:192.168.42.254:255.255.255.0:imx8mm-lpddr4-evk-ml.res.training:eth0:dhcp:192.168.42.254 nfsroot=192.168.42.107:/opt/poky/imx8mm-lpddr4-evk-ml-rootfs,v3,tcp
# cat /proc/net/pnp
# #MANUAL
# nameserver 192.168.42.254
# bootserver 192.168.42.107
#######################################################################################
# so we check here if /proc/net/pnp contains nameserver and if it does 
# with systemd:  we use systemd-resolve to add it for eth0
# with sysvinit: we use resolvconf to add 8.8.8.8 and the custom nameserver
#######################################################################################

#DEVICE="/dev/mmcblk0p3"   DEVICE_FOR_CONTAINER
#MOUNTPOINT="/run/media/mmcblk0p3" MOUNT_FOR_CONTAINER
#SUBVOL1="${MOUNTPOINT}/subvol1" ${MOUNT_FOR_CONTAINER}/${CONTAINER_SUBVOL1}
#SUBVOL2="${MOUNTPOINT}/subvol2" ${MOUNT_FOR_CONTAINER}/snapshot

# debug:
#export DEBUG=on

# no debug:
unset DEBUG

dbg_echo () {
if [ ${DEBUG} = "on" ]; then
   set +x
fi
          [[ "$DEBUG" ]] && builtin echo $@
if [ ${DEBUG} = "on" ]; then
   set -x
fi
}

