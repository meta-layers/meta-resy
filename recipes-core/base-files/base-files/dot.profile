# ~/.profile: executed by Bourne-compatible login shells.

if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi

# path set by /etc/profile
# export PATH

# Might fail after "su - myuser" when /dev/tty* is not writable by "myuser".
mesg n 2>/dev/null

# ---> additions

ping () {
    if hash ccze 2>/dev/null; then
  /bin/ping "$@" | ccze -A;
    else
  /bin/ping "$@";
    fi
}

tail () {
    if hash ccze 2>/dev/null; then
  /usr/bin/tail "$@" | ccze -A -o lookups=no;
    else
  /usr/bin/tail "$@";
    fi
}

journalctl () {
    if hash ccze 2>/dev/null; then
  /bin/journalctl "$@" | ccze -A;
    else
  /bin/journalctl "$@";
    fi
}

systemctl () {
    if hash ccze 2>/dev/null; then
  /bin/systemctl "$@" | ccze -A;
    else
  /bin/systemctl "$@";
    fi
}

networkctl () {
    if hash ccze 2>/dev/null; then
  /bin/networkctl "$@" | ccze -A;
    else
  /bin/networkctl "$@";
    fi
}

dmesg () {
    if hash ccze 2>/dev/null; then
  /bin/dmesg "$@" | ccze -A;
    else
  /bin/dmesg "$@";
    fi
}

dmesgclear () {
   /bin/dmesg -c;
}

dmesgsave () {
    if hash ccze 2>/dev/null; then
  sh -c "/bin/dmesg | ccze -h > ~/log.html";
    else
  echo "I need ccze for this"
    fi
}

# systemd-cgls is is bad function name
# so I renamed it to systemd_cgls
systemd_cgls () {
    if hash ccze 2>/dev/null; then
  /usr/bin/systemd-cgls "$@" | ccze -A;
    else
  /usr/bin/systemd-cgls "$@";
    fi
}

# I guess like this I get what I want ;)
alias systemd-cgls="systemd_cgls"

# <--- additions

# --> TERM
# we set TERM, otherwise serial console does not work properly
# 1) vim does not work properly without it from
# 2) byobu function keys don't work without it
#export TERM="xterm-256color"
export TERM="screen"
# <-- TERM
