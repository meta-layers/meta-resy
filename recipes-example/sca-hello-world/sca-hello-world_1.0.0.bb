# SPDX-FileCopyrightText: 2021 Robert Berger <RobertBerger@ReliableEmbeddedSystems.com>
#
# SPDX-License-Identifier: MIT

# Proper header as required by oelint
SUMMARY = "A simple hello world C application"
DESCRIPTION = "Simple helloworld application"
AUTHOR = "Robert Berger  <robert.berger@ReliableEembeddedSystems.com>"
HOMEPAGE = "https://www.ReliableEmbeddedSystems.com"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://sca-hello-world.c"

# (yocto default) security compiler and linker flags 
# conf/distro/include/security_flags.inc
# are already included by default
# let's include for this recipe "more security flags"
require conf/distro/include/more_security_flags.inc

# S should be placed before inherit
S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"

# sca checks which apply to this recipe
inherit sca-c-checks

# only inherit sca if set in DISTRO_FEATURES
# inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

# in case we have forcecompile defined in DISTRO_FEATURES include a file, which enforces compile -> sca
require ${@ bb.utils.contains("DISTRO_FEATURES", "forcecompile", "conf/distro/include/force-compile.inc" , "", d)}

CFLAGS += "${SECURITY_CFLAGS} ${MORE_SECURITY_CFLAGS}"
LDFLAGS += "${SECURITY_LDFLAGS} ${MORE_SECURITY_LDFLAGS}"

do_compile() {
# with this line it's what's currently the default CFLAGS/LDFLAGS
#       ${CC} -Werror sca-hello-world.c ${CFLAGS} ${LDFLAGS} -o sca-hello-world
# wit this line it will break the compilation due to -Werror and missing CFLAGS
#       ${CC} -Werror sca-hello-world.c ${LDFLAGS} -o sca-hello-world
# with this line we provoke extra gcc sca warnings because CFLAGS are missing
        ${CC} sca-hello-world.c ${LDFLAGS} -o sca-hello-world
}

do_install() {
        install -d ${D}${bindir}
        install -m 0755 sca-hello-world ${D}${bindir}
}

# Note: added ${LDFLAGS} in morty to get rid of:
#       do_package_qa: QA Issue: No GNU_HASH in the elf binary
