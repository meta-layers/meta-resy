#
# Copyright (C) 2021 Robert Berger - Reliable Embedded Systems e.U.
# SPDX-License-Identifier: MIT
#

SRCREV = "56bb573b17abf8256aae363ff3e07b48c2c89549"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a50d0469eb02a4651c9625d696b22a69"

