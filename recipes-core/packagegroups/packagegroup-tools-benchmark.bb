DESCRIPTION = "Tools Benchmark Package Group"

inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "\
    packagegroup-tools-benchmark"

# iozone3
# useful for determining a broad benchmark of filesystem performance
# The benchmark tests file I/O performance for the following operations:
# Read, write, re-read, re-write, read backwards, read strided, fread,
# fwrite, random read/write, pread/pwrite variants.

RDEPENDS:${PN} = "\
iozone3 \
"

# iperf
# The ultimate speed test tool for TCP, UDP and SCTP

RDEPENDS:${PN} += "\
iperf2 \
iperf3 \
"

# @@@ TODO: I'll deal with it later
# Does not want to compile with riscv64
# RDEPENDS:${PN} += "\
# libhugetlbfs\
#"

# memtester
# stress test to find memory subsystem faults.

RDEPENDS:${PN} += "\
memtester \
"

# nbench-byte
# BYTEmark, is a synthetic computing benchmark program developed in the mid-1990s
# by the now defunct BYTE magazine intended to measure a computer's CPU, FPU, and Memory System speed.

RDEPENDS:${PN} += "\
nbench-byte \
"

# tinymembench
# This benchmark tests the system memory (RAM) performance.

RDEPENDS:${PN} += "\
tinymembench \
"

# stress-ng
# a tool to load and stress a computer system

RDEPENDS:${PN} += "\
stress-ng \
"

# phoronix-test-suite
# the most comprehensive testing and benchmarking platform available
# that provides an extensible framework for which new tests can be easily added.
# Please note that you will need a native toolchain installed
RDEPENDS:${PN} += "\
phoronix-test-suite \
"


RDEPENDS:${PN}:append:arm = " cpuburn-neon \
    cpuburn-arm \
    "

# not for x86:
# cpuburn-neon
# cpuburn-arm

# add those if you need them:
#    analyze-suspend 
#    bonnie++ 
#    dbench 
#    fio 
#    glmark2 
#    lmbench 
#    tiobench 
#    stress

# --> riscv64
# this does not work:
#RDEPENDS:${PN}:remove:riscv64 = "libhugetlbfs"
# <-- riscv64

