# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/core-image-minimal-virt-k3s-common.bb

IMAGE_INSTALL += "\
packagegroup-k3s-host \
"
