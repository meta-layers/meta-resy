# in order to play with rootless containers:
# - create a non root user/group

# --> user/group
inherit useradd

# --> podman group
# --system                  create a system account
# This should be a "normal account" and not a system account
# difference is only uid/gid range

# groupadd podman

GROUPADD_PARAM:${PN} = "podman"

# groupadd podman
# <-- podman group

# --> podman user - encryted password in recipe
# --system                  create a system account
# --gid GROUP               name or ID of the primary group of the new
#                           account
# This should be a "normal account" and not a system account
# difference is only uid/gid range

# how to create encrypted password, which we need here?
#
# !!!!!!!!!!!!!!! the plain text password shcould not be in the recipe !!!!!!!!!!!!!!!!
# this should normally not be public, since it's the plain text password in the explanation
#
# openssl passwd -6 -salt ReliableEmbeddedSystems podman
# $6$ReliableEmbedded$sf.hOOf4hepM7Bnya12fA6fpHddVe0GpqdMkIKd4Z/vVai8mFjMT1qckKBjAb220cGKwV/Dkqu85200fT4.2Y.
# 
# useradd podman -p '$6$ReliableEmbedded$sf.hOOf4hepM7Bnya12fA6fpHddVe0GpqdMkIKd4Z/vVai8mFjMT1qckKBjAb220cGKwV/Dkqu85200fT4.2Y.' --gid podman
# now we need to escape a few things for this to work ;)

USERADD_PARAM:${PN} += "podman \
                        -p '\$6\$ReliableEmbedded\$sf.hOOf4hepM7Bnya12fA6fpHddVe0GpqdMkIKd4Z/vVai8mFjMT1qckKBjAb220cGKwV/Dkqu85200fT4.2Y.' \
                        --gid podman \
                       "
# <-- podman user - encrypted password in recipe

# --> podman user - plain text password in recipe
### let's try with a plain text password:
# you'll need to follow proper precautions to ensure your recipe 
# stays secret since it includes the plaintext password:

#USERADD_PARAM:${PN} += "podman \
#                        -P 'podman' \
#                        --gid podman \
#                       "
# Note that this is what's ends up now in /etc/shadow
#
# root@multi-v7-ml:~# cat /etc/shadow | grep podman
# podman:$6$PG1S49bB$GUXnAEdVMPM0Pgd4VyAJ05A7o.ZbgEG37Kp.By9pzWlErfQvEdKQX9MytvrkYfR2k7iu4Z5r4RA8nJ0Ym1hS30::0:99999:7:::
# <-- podman user - plain text password in recipe

# --> add it to the podman package
# USERADD_PACKAGES specifies the output packages
# which include custom users/groups.
# let's add the new user to the podman package
USERADD_PACKAGES = "${PN}"
# <-- add it to the podman package

######### testing/debugging ########

# How to check that this works?

# 1) 
# With buildhistory the pre/post install/remove scripts are easily accessible
# Just copy latest.pkg_preinst to the target and run it to see that it works

# 2)
# Make sure you can log in with podman/podman afterwards e.g. via ssh

######### hardening #########

# 1)
# Once this works you can force the podman user to change the password on the first login
# with chageusers.bbclass from local.conf?
