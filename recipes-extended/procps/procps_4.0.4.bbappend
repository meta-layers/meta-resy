FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-${PV}:"

# patches against the UNPACKDIR!
SRC_URI += "\
           file://0001-fix-for-CVE-2019-14899.patch;patchdir=${UNPACKDIR} \
"

# linux-yocto-custom CVE-2019-14899    4.9      7.4      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2019-14899
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2019-14899
# upstream_linux: deferred (2019-12-13)
#
# https://ubuntu.com/security/CVE-2019-14899
# No current fix from upstream as of 2019-12-13
# it is asserted that the Linux XFRM IPsec implementation does not allow bypassing by routing.
#
# https://security-tracker.debian.org/tracker/CVE-2019-14899
# A vulnerability was discovered in Linux, FreeBSD, OpenBSD, MacOS, iOS, and Android that allows a malicious access point, or an adjacent user,
# to determine if a connected user is using a VPN, make positive inferences about the websites they are visiting,
# and determine the correct sequence and acknowledgement numbers in use, allowing the bad actor to inject data into the TCP stream.
# This provides everything that is needed for an attacker to hijack active connections inside the VPN tunnel.
#
# https://www.openwall.com/lists/oss-security/2019/12/05/1
#
# **Possible Mitigations:
#
# !!!!! 1. Turning reverse path filtering on !!!!!! - see below
#
# Potential problem: Asynchronous routing not reliable on mobile devices,
# etc. Also, it isn’t clear that this is actually a solution since it
# appears to work in other OSes with different networking stacks. Also,
# even with reverse path filtering on strict mode, the first two parts of
# the attack can be completed, allowing the AP to make inferences about
# active connections, and we believe it may be possible to carry out the
# entire attack, but haven’t accomplished this yet.
#
# 2. Bogon filtering
#
# Potential problem: Local network addresses used for vpns and local
# networks, and some nations, including Iran, use the reserved private IP
# space as part of the public space.
#
# 3. Encrypted packet size and timing
#
# Since the size and number of packets allows the attacker to bypass the
# encryption provided by the VPN service, perhaps some sort of padding
# could be added to the encrypted packets to make them the same size.
# Also, since the challenge ACK per process limit allows us to determine
# if the encrypted packets are challenge ACKs, allowing the host to
# respond with equivalent-sized packets after exhausting this limit could
# prevent the attacker from making this inference.
#
#
# We have prepared a paper for publication concerning this
# vulnerability and the related implications, but intend to keep it
# embargoed until we have found a satisfactory workaround. Then we will
# report the vulnerability to oss-security@...ts.openwall.com. We are
# also reporting this vulnerability to the other services affected, which
# also includes: Systemd, Google, Apple, OpenVPN, and WireGuard, in
# addition to distros@...openwall.org for the operating systems affected.
#
# https://wiki.yoctoproject.org/wiki/CVE_Status#CVE-2019-14899_(linux-yocto)
# Claims to be about breaking into VPN tunnels. OpenVPN dispute, Red Hat think it might actually have a larger scope but also the paper is misleading.
#
# https://support.sophos.com/support/s/article/KBA-000007138?language=en_US
# Sophos has confirmed that the XG and UTM firewall devices are not affected by this as they utilize policy-based VPN technology and the threat only affects route-based VPNs.
# The Sophos SSL VPN client is not affected as it is based on OpenVPN. As per OpenVPN, the software is not affected.
#
# https://seclists.org/oss-sec/2019/q4/123
# Hello List,
#
# Some important comments on the matter and especially in regards to IPsec:
# * This attack works regardless of if you have a VPN or not. The attacker just needs to be able to
#   send packets to the other host. It's not systemd specific. It can also occur because the user deliberately
#   configured the rp_filter that way (that's sometimes the case if PBR (Policy Based Routing) is configured.
#   The default for rp_filter is strict. For further information on the matter see ip-sysctl.txt[2]
#   and RFC 3704 Section 2.4[3]. For now, just create a file /etc/sysctl.d/51-rpfilter.conf with the content
#   "net.ipv4.conf.all.rp_filter=1".
# * You can solve the problem generally for IPv6 by using the rpfilter iptables or nftables module in *mangle
#   PREROUTING[1].
#   Just globally one rule is needed.
# * Only route based VPNs are impacted. In comparison, policy based VPNs are not impacted (On Linux only implementable
#   using XFRM, which
#   is IPsec on Linux specific) unless the XFRM policy's level is set to "use" instead of "required" (default))
#   because any traffic received that matches a policy (IPsec security policy) and that is not protected is dropped.
#   An attacker could only inject packets by attacking the connection whenever it is unprotected (e.g. On a commercial
#   VPN provider
#   setup that would be when the connection "comes" out of the VPN server and goes to the destination on the WAN).
#   So you're ususally fine. And even when a route based VPN is used, strict rp_filter can still save your bacon.
#
# The probing of "virtual" IPv4 addresses can be made more difficult by configuring the VPN software to bind them to,
# for example, the loopback interface and setting arp_ignore of any interface facing a possible attacker to 2[2].
# That would prevent the sending of arp responses to arp requests for virtual IPs. I am not aware of a
# similiar setting for IPv6. That might be related to the lack of an rp_filter setting for IPv6 on Linux.
# Probing for addresses by using any protocol other than TCP (because TCP on Linux is handled in a special way
# in regards to routing. It sends the responses over the same interface and MAC addresses as the request was received,
# AFAIR) would not be possible if the response was to go over the VPN tunnel because the VPN server would most likely
# drop it as a martian (the destination would probably be a private network and they're not routable over the Internet.
# It has to be investigated on a case by case basis).
#
# strongSwan by default binds any "virtual" IPs to the interface the route to the other peer goes over. You can change
# that though.
# I don't know about libreswan or openswan (shouldn't use the last one anyway).
#
# This vulnerability works against OpenVPN, WireGuard, and IKEv2/IPSec,
# but has not been thoroughly tested against tor, but we believe it is
# not vulnerable since it operates in a SOCKS layer and includes
# authentication and encryption that happens in userspace.
#
# It doesn't work against TOR because the destination address would be 127.0.0.1 and 
# Linux (don't know about other operating systems) drops packets to that destination unless the input
# interface is loopback or route_localnet in sysctl of the input interface is set to 1 (used if services
# bound to localhost are exposed to the network via DNAT rules).
#
# 3. Encrypted packet size and timing
#
# Since the size and number of packets allows the attacker to bypass the
# encryption provided by the VPN service, perhaps some sort of padding
# could be added to the encrypted packets to make them the same size.
# Also, since the challenge ACK per process limit allows us to determine
# if the encrypted packets are challenge ACKs, allowing the host to
# respond with equivalent-sized packets after exhausting this limit could
# prevent the attacker from making this inference.
#
# IPsec supports that. It's called TFC (Traffic Flow Confidentiality). It can be configured to arbitrary values or to pad
# up to the MTU of the link.
# It's disabled by default.
#
# Kind regards
#
# Noel
#
# [1] Would look like that: ip6tables -t mangle -I PREROUTING -m rpfilter --invert -j DROP
# [2] https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt
# [3] https://tools.ietf.org/html/rfc3704#section-2.4
#
# https://security.stackexchange.com/questions/237391/wireguard-cve-2019-14899-how-secure-the-protocol-really-is
#
# https://forum.netgate.com/topic/148713/cve-2019-14899/9
#
# https://marc.info/?l=oss-security&s=CVE-2019-14899
#
# from OpenVPN:
# “The issue may actually be located in the Linux operating system settings rather than in our software,
#  but given the serious nature of the attack, we are paying close attention and will consider whatever
#  steps are appropriate to ensure OpenVPN remains safe to use on these affected platforms.
#  For now enabling the ‘reverse path filter’ setting in the OS is a good first step to help protect against this attack,” says Draaisma.
#
#  https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/6/html/security_guide/sect-security_guide-server_security-reverse_path_forwarding#sect-Security_Guide-Server_Security-rp_filter-resources
#
#  https://access.redhat.com/solutions/53031
#
#  $ sysctl -a 2>/dev/null | grep "\.rp_filter"
#  net.ipv4.conf.all.rp_filter = 1
#  net.ipv4.conf.default.rp_filter = 0
#  net.ipv4.conf.enp0s31f6.rp_filter = 0
#  net.ipv4.conf.lo.rp_filter = 0
#  net.ipv4.conf.tun0.rp_filter = 0
#  net.ipv4.conf.virbr0.rp_filter = 0
#  net.ipv4.conf.virbr0-nic.rp_filter = 0
#  net.ipv4.conf.virbr1.rp_filter = 0
#  net.ipv4.conf.virbr1-nic.rp_filter = 0
#  net.ipv4.conf.wlp58s0.rp_filter = 0
#
#  The filtering method is controlled globally by the sysctl net.ipv4.conf.all.rp_filter described in the kernel documentation:
#  https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt.
#  RHEL 6+ override the kernel default value of 0 (disabled) for this parameter and set it to 1 (strict).
#
#  rp_filter - INTEGER
#    0 - No source validation.
#    1 - Strict mode as defined in RFC3704 Strict Reverse Path
#        Each incoming packet is tested against the FIB and if the interface
#        is not the best reverse path the packet check will fail.
#        By default failed packets are discarded.
#    2 - Loose mode as defined in RFC3704 Loose Reverse Path
#        Each incoming packet's source address is also tested against the FIB
#        and if the source address is not reachable via any interface
#        the packet check will fail.
#
#    Current recommended practice in RFC3704 is to enable strict mode
#    to prevent IP spoofing from DDos attacks. If using asymmetric routing
#    or other complicated routing, then loose mode is recommended.
#
#    The max value from conf/{all,interface}/rp_filter is used
#    when doing source validation on the {interface}.
#
#    Default value is 0. Note that some distributions enable it
#    in startup scripts.
#
# https://sysctl-explorer.net/net/ipv4/rp_filter/
# Nb: per interface setting (where “interface” is the name of your network interface); 
# "all" is a special interface: changes the settings for all interfaces.
#
# Remediation:
# https://docs.datadoghq.com/security/default_rules/xccdf-org-ssgproject-content-rule-sysctl-net-ipv4-conf-default-rp-filter/
#
# set net.ipv4.conf.all.rp_filter = 1 (strict)
#
# for fix see meta/recipes-extended/procps/procps
#
# check:
# sudo /sbin/sysctl -A | grep "\.rp_filter"
#
# more explanation: https://forum.openwrt.org/t/help-understanding-rp-filter-as-applied-via-sysctl-conf/197823/5
#
# CVE_STATUS[CVE-2019-14899] = "upstream-wontfix: CVE from 2019 is reported against kernel, fix is in sysctl.conf: net.ipv4.conf.all.rp_filter = 1"
# 
# !!! the CVE_STATUS is set in the kernel recipe !!!
