DESCRIPTION = "Tools Commandline Package Group"

inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "\
    packagegroup-tools-cmdline"

RDEPENDS:packagegroup-tools-cmdline = "\
    pv \
    tree \
    vim \
    timelimit \
    lsof \
    psmisc \
    ccze \
    "

# don't build ccze if we build with musl for now
# proper fix would be to fix the ccze recipe to build with musl
RDEPENDS:packagegroup-tools-cmdline:remove:libc-musl = "ccze"

# normally I want it in with glibc:
# RDEPENDS:packagegroup-tools-cmdline:remove:libc-glibc = "ccze"
