# --> that's what's in the native rootfs and the container
# Note that busybox is required to satify /bin/sh requirement of mosquitto,
# and the access* modules need to be explicitly specified since RECOMMENDATIONS
# are disabled.
IMAGE_INSTALL += " \
	busybox \
	phoronix-test-suite \
"

IMAGE_INSTALL += " \
        packagegroup-core-base-utils \
"

# the phoronix-test-suite needs a toolchain to build stuff -> tools-sdk
# tools-sdk is defined in the core-image.bbclass
inherit core-image
IMAGE_FEATURES += "dev-pkgs tools-sdk"
# <-- that's what's in the native rootfs and the container
