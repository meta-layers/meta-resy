# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "A tensorflow container image"

require recipes-core/images/app-container-image.bb
require dynamic-layers/meta-tensorflow/recipes-core/images/app-container-image-tensorflow-common.inc
