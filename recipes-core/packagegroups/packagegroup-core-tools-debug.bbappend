RDEPENDS:${PN}:append = "\
    smem \
    crash \
    valgrind \
    procrank-linux \
    makedumpfile \
    "
# removed:
# minicoredumper
# added:
# crash-small 8.0.4
# crash-small

# https://www.selenic.com/smem/

# --> aarch64
RDEPENDS:${PN}:remove:aarch64 = "crash-small"
# <-- aarch64

# --> riscv64
RDEPENDS:${PN}:remove:riscv64 = "crash"

RDEPENDS:${PN}:remove:riscv64 = "crash-small"

RDEPENDS:${PN}:remove:riscv64 = "valgrind"

RDEPENDS:${PN}:remove:riscv64 = "makedumpfile"
# <-- riscv64
