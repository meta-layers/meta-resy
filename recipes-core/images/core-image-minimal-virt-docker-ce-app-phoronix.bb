# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

# --> that's what's in the native rootfs

require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL += "\
                  dropbear \
                  docker-ce \
                  docker-ce-contrib \
                  e2fsprogs-mke2fs \
                  bash \
                  btrfs-tools \
                  udev-extraconf \
                  python3 \
                  python3-docker-compose \
"

# glibc-gconv-utf-16
# softether

# udev-extraconf for automount

IMAGE_INSTALL += "\
                  packagegroup-tools-cmdline \
                  packagegroup-tools-top \
                  packagegroup-tools-rt \
"

# packagegroup-tools-io


IMAGE_INSTALL += "\
		 ntp \
                 "
# mosquitto-clients

# <-- that's what's in the native rootfs
# --> that's what's in the native rootfs and the container

# attempt to have it as an app container as well as a native image
# so we can do benchmarks
require recipes-core/images/app-container-image-phoronix-common.inc
# <-- that's what's in the native rootfs and the container

