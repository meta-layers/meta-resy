# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "A lighttpd container image packaged to go on the rootfs and be loaded by docker"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# --> ====== multiconfig magic =====

RESY_CONTAINER = "resy-container"

NATIVE_MACHINE ??= "imx6q-phytec-mira-rdk-nand-resy-virt"

CONTAINER_APP="lighttpd"
CONTAINER_SPECIFIC_PATH="${RESY_CONTAINER}/${CONTAINER_APP}"

CONTAINER_MACHINE="container-arm-v7"
CONTAINER_DISTRO="resy-container"
CONTAINER_IMAGE="app-container-image-${CONTAINER_APP}-oci"
CONTAINER_TMP="tmp-${CONTAINER_MACHINE}-${CONTAINER_DISTRO}"
CONTAINER_TAR_BZ2="app-container-image-${CONTAINER_APP}-oci-container-arm-v7.tar.bz2"
CONTAINER_TAR_BZ2_PATH="${CONTAINER_TMP}/deploy/images/${CONTAINER_MACHINE}"
CONTAINER_INSTALL_PATH="${RESY_CONTAINER}/prepopulated-container-images"

CONTAINER_DOCKER_COMPOSE="${RESY_CONTAINER}/docker-compose"
CONTAINER_SPECIFIC_PATH="${RESY_CONTAINER}/${CONTAINER_APP}"

# syntax:
#task_or_package[mcdepends] = "mc:from_multiconfig:to_multiconfig:recipe_name:task_on_which_to_depend"
do_fetch[mcdepends] = "mc:${NATIVE_MACHINE}:${CONTAINER_MACHINE}-${CONTAINER_DISTRO}:${CONTAINER_IMAGE}:do_image_complete"

# <-- ===== multiconfig magic =====

SRC_URI += " \
    file://${TOPDIR}/${CONTAINER_TAR_BZ2_PATH}/${CONTAINER_TAR_BZ2};unpack=0 \
    file://index.html \
    file://docker-compose.yml \
"
FILES:${PN} += "/${CONTAINER_INSTALL_PATH}/${CONTAINER_TAR_BZ2} \
                /${CONTAINER_SPECIFIC_PATH}/www/pages/index.html \
                /${CONTAINER_SPECIFIC_PATH}/www/pages/dav \
                /${CONTAINER_DOCKER_COMPOSE}/${CONTAINER_APP}/docker-compose.yml \
"

do_install() {
	# place in rootfs
       	install -d ${D}/${rootdir}/${CONTAINER_INSTALL_PATH}
        install -m 0444 ${WORKDIR}/${TOPDIR}/${CONTAINER_TAR_BZ2_PATH}/${CONTAINER_TAR_BZ2} \
           ${D}/${rootdir}/${CONTAINER_INSTALL_PATH}/${CONTAINER_TAR_BZ2}
	# lighttpd specific stuff - so we can have index.html from host - volume mounted
        install -d ${D}/${rootdir}/${CONTAINER_SPECIFIC_PATH}/www/pages/dav
        install -m 0444 ${WORKDIR}/index.html \
           ${D}/${rootdir}/${CONTAINER_SPECIFIC_PATH}/www/pages/index.html
        # for now only one docker-compose
        install -d ${D}/${rootdir}/${CONTAINER_DOCKER_COMPOSE}/${CONTAINER_APP}
        install -m 0444 ${WORKDIR}/docker-compose.yml \
           ${D}/${rootdir}/${CONTAINER_DOCKER_COMPOSE}/${CONTAINER_APP}/docker-compose.yml
}

# --> systemd service
REQUIRED_DISTRO_FEATURES= "systemd"
inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
# disable for manual testing
# e.g. on target:
# systemctl start docker-compose-lighttpd
# SYSTEMD_AUTO_ENABLE = "disable"
SYSTEMD_SERVICE:${PN} = "docker-compose-lighttpd.service"

SRC_URI += "file://docker-compose-lighttpd.service"

FILES:${PN} += "${systemd_unitdir}/system/docker-compose-lighttpd.service"

do_install:append () {
	install -d ${D}${systemd_unitdir}/system
	install -c -m 0644 ${WORKDIR}/docker-compose-lighttpd.service ${D}${systemd_unitdir}/system
}
# <-- systemd service

# COMPATIBLE_HOST[doc] = "A regular expression that resolves to one or more hosts (when the recipe is native) 
#                         or one or more targets (when the recipe is non-native) with which a recipe is compatible."
COMPATIBLE_HOST = '(arm.*)-(linux.*|freebsd.*)'

#${WORKDIR}/
#workdir
#        └── build
#            └── imx6q-phytec-mira-rdk-nand-virt-wic-mc
#                └── tmp-container-arm-v7-resy-container
#                    └── deploy
#                        └── images
#                            └── container-arm-v7
#                                └── app-container-image-lighttpd-oci-container-arm-v7.tar.bz2

# lighttpd structure:
# tree /www
# /www
# `-- pages
#    |-- dav
#    |-- index.html


#pkg_postinst_ontarget:${PN}() {
# docker version
# docker import /prepopulated-container-images/app-container-image-lighttpd-oci-container-arm-v7.tar.bz2 reliableembeddedsystems/oci-lighttpd:latest
#}

# I guess we can try some systemd service for this
#
# 1) use docker import for prepopulated stuff
# docker image inspect reliableembeddedsystems/oci-lighttpd:latest >/dev/null 2>&1 && reliableembeddedsystems/oci-lighttpd:latest exists && || echo does not exist && docker import /prepopulated-container-images/app-container-image-lighttpd-oci-container-arm-v7.tar.bz2 reliableembeddedsystems/oci-lighttpd:latest

# 2) use docker-compose to start up the beast
#
# docker-compose.yml:
#
# version: '3'
# 
# services:
#  lighttpd:
#    image: reliableembeddedsystems/oci-lighttpd:latest
#    container_name: lighttpd
#    ports:
#      - "8080:80"
#    volumes:
#      - /var/lib/www:/var/lib/www
#    entrypoint: ["/bin/sh", "-c", "/etc/init.d/lighttpd restart && tail -f /var/log/access.log"]
#
#    # this is how we would call it directly from docker:
#    # docker run -p ${PUBLIC_PORT}:80 --entrypoint=/bin/sh ${IMAGE_NAME} -c '/etc/init.d/lighttpd restart && tail -f /var/log/access.log'

# shell in the container:
#
# cd /docker-compose/
# root@imx6q-phytec-mira-rdk-nand:/docker-compose# docker-compose exec lighttpd /bin/sh

# systemd service template:
# /etc/systemd/system/docker-compose@.service
#
#[Unit]
#Description=docker-compose %i service
#Requires=docker.service network-online.target
#After=docker.service network-online.target
#
#[Service]
#WorkingDirectory=/etc/docker-compose/%i
#Type=simple
#TimeoutStartSec=15min
#Restart=always
#
#ExecStartPre=/usr/local/bin/docker-compose pull --quiet --ignore-pull-failures
#ExecStartPre=/usr/local/bin/docker-compose build --pull
#
#ExecStart=/usr/local/bin/docker-compose up --remove-orphans
#
#ExecStop=/usr/local/bin/docker-compose down --remove-orphans
#
#ExecReload=/usr/local/bin/docker-compose pull --quiet --ignore-pull-failures
#ExecReload=/usr/local/bin/docker-compose build --pull
#
#[Install]
#WantedBy=multi-user.target


###### what I used so far with grafana: 

# [Unit]
# Description=Docker Compose container starter %i
# After=docker.service network-online.target
# Requires=docker.service network-online.target

# [Service]
##WorkingDirectory=/root/dockers/%i
#WorkingDirectory=/home/student/projects/%i
#Type=oneshot
#RemainAfterExit=yes

##ExecStartPre=-/usr/local/bin/docker-compose pull --quiet
#ExecStart=/usr/bin/docker-compose up -d

#ExecStop=/usr/bin/docker-compose down

##ExecReload=/usr/bin/docker-compose pull --quiet
#ExecReload=/usr/bin/docker-compose up -d

#[Install]
#WantedBy=multi-user.target

# let's try that -->

#[Unit]
#Description=docker-compose %i service 
#After=docker.service network-online.target
#Requires=docker.service network-online.target
#
#[Service]
#WorkingDirectory=/resy-container/docker-compose/%i
#Type=simple
#TimeoutStartSec=15min
#Restart=always
#
## with ExecStartPre we might the containef from file 
##ExecStartPre=/usr/bin/docker-compose pull --quiet --ignore-pull-failures
## we don't build here
##ExecStartPre=/usr/local/bin/docker-compose build --pull
#
##ExecStart=/usr/bin/docker-compose up --remove-orphans
##ExecStop=/usr/bin/docker-compose down --remove-orphans
#
#ExecStart=/usr/bin/docker-compose up -d
#ExecStop=/usr/bin/docker-compose down
#
##ExecReload=/usr/bin/docker-compose pull --quiet --ignore-pull-failures
##ExecReload=/usr/bin/docker-compose build --pull
#ExecReload=/usr/bin/docker-compose up -d
#
#[Install]
#WantedBy=multi-user.target

# <-- let's try that

#systemctl start docker-compose@lighttpd

# systemctl start docker-compose-lighttpd

#and voila


