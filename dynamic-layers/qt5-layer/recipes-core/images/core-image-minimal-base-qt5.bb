# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/core-image-minimal-base.bb

# qt support in SDK
inherit populate_sdk_qt5

IMAGE_INSTALL += "\
                  qtbase \
                  qtserialport \
                  hello-qt \
"

# global - in local.conf
#DISTRO_FEATURES:remove = "wayland"
#DISTRO_FEATURES:remove = "vulkan"

# this is a pure qt5 embedded demo image without X
CONFLICT_DISTRO_FEATURES = "x11 wayland"

# enable stuff from MACHINE/DISTRO_FEATURES
# I added this in local.conf, since it's small
#CORE_IMAGE_EXTRA_INSTALL += "packagegroup-base-extended"
