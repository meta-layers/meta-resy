# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/common-container-img.inc

CORE_IMAGE_EXTRA_INSTALL += " podman conmon"

# nsenter (util-linux) is needed by podman-compose
# ip6tables (iptables) is needed by podman-compose
# both should already be included from common-container-img.inc
CORE_IMAGE_EXTRA_INSTALL += " util-linux iptables"
CORE_IMAGE_EXTRA_INSTALL += " podman-compose"

#IMAGE_INSTALL += "\
#                 python3-docker-compose \
#"
