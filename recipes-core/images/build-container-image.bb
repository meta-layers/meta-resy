# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Bootstrap container"
DESCRIPTION = "A minimal bootstrap container image"

IMAGE_FSTYPES = "container"

inherit core-image

# packagegroup-self-hosted.bb:
#   Packages required to run the build system

# the others are part of the .bb file above

IMAGE_INSTALL = " \
	packagegroup-core-boot \
	packagegroup-self-hosted-sdk \
	packagegroup-self-hosted-extended \
	${CORE_IMAGE_EXTRA_INSTALL} \
"

IMAGE_LINGUAS = "en-us"
IMAGE_TYPEDEP:container += "ext4"

# Workaround /var/volatile for now
ROOTFS_POSTPROCESS_COMMAND += "rootfs_fixup_var_volatile ; "

rootfs_fixup_var_volatile () {
	install -m 1777 -d ${IMAGE_ROOTFS}/${localstatedir}/volatile/tmp
	install -m 755  -d ${IMAGE_ROOTFS}/${localstatedir}/volatile/log
}

#IMAGE_INSTALL:remove += " webkitgtk "
