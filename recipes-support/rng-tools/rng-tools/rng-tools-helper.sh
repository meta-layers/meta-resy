#!/bin/sh
#
# SPDX-License-Identifier: GPL-2.0-only
#

# check if kernel module is loaded:
function moduleExist(){
  MODULE="$1"
  if lsmod | grep "$MODULE" &> /dev/null ; then
    return 0
  else
    return 1
  fi
}

# Disabling 6: JITTER Entropy generator (jitter)
if moduleExist "caam"; then
   echo "the caam module is loaded - let's use it instead of maxing out the CPUs"
   EXTRA_ARGS="${EXTRA_ARGS} -x jitter"
fi
