#
# SPDX-License-Identifier: MIT
#
# Shell In A Box implements a web server that 
# can export arbitrary command line tools to 
# a web based terminal emulator. This emulator 
# is accessible to any JavaScript and CSS 
# enabled web browser and does not require any additional browser plugins.
# https://github.com/shellinabox/shellinabox
#
PV = "2.21+git${SRCPV}"

DESCRIPTION = "Shell In A Box implements a web server that \
can export arbitrary command line tools to \
a web based terminal emulator."

HOMEPAGE = "https://github.com/shellinabox/shellinabox"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=a193d25fdef283ddce530f6d67852fa5"

inherit autotools-brokensep
inherit pkgconfig

DEPENDS += "zlib"

GIT_PROTOCOL = "https"
SRC_URI = "git://github.com/shellinabox/shellinabox.git;protocol=${GIT_PROTOCOL};branch=master"
SRCREV = "4f0ecc31ac6f985e0dd3f5a52cbfc0e9251f6361"

# Files
SRC_URI += "\
	file://shellinabox.css \
"

# file://shellinabox.init
# file://shellinabox.config
# file://shellinabox.keep

# sys-v not supported at the moment
SRC_URI:append = " ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'file://shellinabox.service', 'file://init.d-shellinabox-server', d)}"

# Patches
SRC_URI += "\
	file://0001-Add-cert-file-option.patch \
"

PACKAGECONFIG ??= "ssl"
PACKAGECONFIG[ssl] = "--enable-ssl,--disable-ssl,openssl"

RDEPENDS:${PN} += "\
	${@bb.utils.contains('PACKAGECONFIG', 'ssl', 'openssl-bin', '', d)} \
"

EXTRA_OECONF += " --disable-runtime-loading"

do_install:append() {
	install -d -m 0755 ${D}${sysconfdir}/shellinabox
	install -d -m 0755 ${D}${sysconfdir}/shellinabox/ssl

	install -m 0644 ${B}/shellinabox/black-on-white.css ${D}${sysconfdir}/shellinabox/
	install -m 0644 ${B}/shellinabox/white-on-black.css ${D}${sysconfdir}/shellinabox/
	install -m 0644 ${UNPACKDIR}/shellinabox.css ${D}${sysconfdir}/shellinabox/

        # Only install the script if 'systemd' is in DISTRO_FEATURES
        # --> systemd
        if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd','true','false',d)}; then
           install -d ${D}${systemd_unitdir}/system
           install -m 0644 ${UNPACKDIR}/shellinabox.service ${D}${systemd_system_unitdir}/shellinabox.service
        fi
        # <-- systemd

        # --> sysvinit
        if ${@bb.utils.contains('DISTRO_FEATURES','sysvinit','true','false',d)}; then
        install -d ${D}${sysconfdir}/init.d/
        install -D -m 0755 ${UNPACKDIR}/init.d-shellinabox-server ${D}${sysconfdir}/init.d/shellinaboxd
        fi 
        # <-- sysvinit
}

S = "${WORKDIR}/git"

inherit systemd update-rc.d

# --> systemd service
# please note, that above
#   we already copy files depeding on sysvinit/systemd
#   we already inherited systemd
SYSTEMD_AUTO_ENABLE = "enable"
# disable for manual testing
# e.g. on target:
# systemctl start shellinabox.service
#SYSTEMD_AUTO_ENABLE = "disable"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "${PN}.service"
# <-- systemd service

# --> sysvinit scripts
# please note, that above
#   we already copy files depeding on sysvinit/systemd
#   we already inherited update-rc.d
INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME:${PN} = "shellinaboxd"
# script has a runlevel of: 99
# starts in initlevels:     2 3 4 5
# stops  in initlevels: 0 1         6
INITSCRIPT_PARAMS:${PN} = "start 95 2 3 4 5 . stop 95 0 1 6 ."
# <-- sysvinit scripts


