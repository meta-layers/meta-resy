#
# Copyright (C) 2021 Robert Berger - Reliable Embedded Systems e.U.
# SPDX-License-Identifier: MIT
#

require version_${PV}.inc
require softether_git.inc
