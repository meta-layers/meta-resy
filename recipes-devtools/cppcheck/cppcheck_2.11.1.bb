SUMMARY = "cppcheck - Static code analyzer for C/C++"
DESCRIPTION = "Cppcheck is a static analysis tool for C/C++ code"
AUTHOR = "The cppcheck team"
HOMEPAGE = "https://cppcheck.sourceforge.io/"
BUGTRACKER = "https://trac.cppcheck.net/"
SECTION = "devel"
LICENSE = "GPL-3.0-only & BSD-2-Clause & BSD-3-Clause"

SRC_URI = "git://github.com/danmar/cppcheck.git;protocol=https;nobranch=1 \
          "
# file://0001-don-t-kill-LDFLAGS.patch 

LICENSE = "GPL-3.0-only & BSD-2-Clause & BSD-3-Clause"
LIC_FILES_CHKSUM = "\
    file://COPYING;md5=d32239bcb673463ab874e80d47fae504 \
    file://externals/simplecpp/LICENSE;md5=959bffe2993816eb32ec4bc1ec1d5875 \
    file://externals/tinyxml2/LICENSE;md5=135624eef03e1f1101b9ba9ac9b5fffd \
    file://externals/picojson/LICENSE;md5=29d6d693711f69885bbfe08072624f2e \
"

SRCREV = "13746898140fa229018b57acdb18091942c8ea05"

S = "${WORKDIR}/git"

inherit pkgconfig

LIBZ3 = "z3"
PACKAGECONFIG ??= "z3"
PACKAGECONFIG[z3] = "USE_Z3=yes,,${LIBZ3}"

do_compile() {
    oe_runmake ${PACKAGECONFIG_CONFARGS} FILESDIR=.
}

do_install() {
    oe_runmake install DESTDIR=${D} FILESDIR=${bindir} PREFIX=${prefix}
}

FILES:${PN} = "${bindir} ${datadir}"
