#!/bin/bash

DEPLOY="beaglebone-yocto-swupdate-master/tmp/deploy/images/beaglebone-yocto-custom"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-full-cmdline-beaglebone-yocto*rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
