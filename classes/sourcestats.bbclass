#########################################################################
# create 2 files:
#	${TMPDIR}/sourcestats.txt
#	${TMPDIR}/sourcestats-native.txt
#
# format:
#	${PN},$files,$size,$spdx
#         |      |     |     |
#         |      |     |      - number of SPDX-License-Identifiers found
#         |      |      ------- size in bytes
#         |       ------------- number of files
#          -------------------- package name
#
########################################################################

SOURCESTATS_FILE = "${TMPDIR}/sourcestats.txt"
SOURCESTATS_FILE:class-native = "${TMPDIR}/sourcestats-native.txt"

do_sourcestats () {    
	cd ${S}    
	files=$(find -type f | wc -l)
	spdx=$(grep SPDX-License-Identifier: -r -I --exclude-dir=temp . 2> /dev/null | wc -l)
	size=$(du -sb . | cut -f 1)
        echo "${PN},$files,$size,$spdx" >> ${SOURCESTATS_FILE}
} 
addtask sourcestats after do_patch before do_prepare_recipe_sysroot
