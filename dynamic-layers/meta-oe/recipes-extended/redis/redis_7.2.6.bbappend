FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-${PV}:"

# patches against the UNPACKDIR!
SRC_URI += "\
           file://0001-sysvinit-vm.overcommit_memory-1.patch;patchdir=${UNPACKDIR} \
           file://0002-systemd-vm.overcommit_memory-1.patch;patchdir=${UNPACKDIR} \
"


# linux-yocto-custom CVE-2022-0543     Unpatched  
#         {
#         "id": "CVE-2022-0543",
#         "status": "Unpatched",
#         "link": "https://nvd.nist.gov/vuln/detail/CVE-2022-0543",
#         "summary": "It was discovered, that redis, a persistent key-value database, due to a packaging issue, is prone to a (Debian-specific) Lua sandbox escape, which could result in remote code execution.",
#         "scorev2": "10.0",
#         "scorev3": "10.0",
#         "scorev4": "0.0",
#         "modified": "2023-09-29T15:55:24.533",
#         "vector": "NETWORK",
#         "vectorString": "AV:N/AC:L/Au:N/C:C/I:C/A:C",
#         "detail": "version-in-range"
#       },

#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-0543
# does not exist
#
# https://ubuntu.com/security/CVE-2022-0543
# Reginaldo Silva discovered that due to a packaging issue, a remote attacker with the ability to execute arbitrary
# Lua scriptss could possibly escape the Lua sandbox and execute arbitrary code on the host.
#
# https://security-tracker.debian.org/tracker/CVE-2022-0543
# https://www.ubercomp.com/posts/2022-01-20_redis_on_debian_rce
CVE_STATUS[CVE-2022-0543] = "not-applicable-platform: Issue only applies on Debian"

