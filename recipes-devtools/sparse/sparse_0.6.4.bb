SUMMARY = "Sparse - a Semantic Parser for C"
HOMEPAGE = "https://sparse.docs.kernel.org"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=69a9605316748b9e191e454efc2235b1"

SRC_URI = "git://git.kernel.org/pub/scm/devel/sparse/sparse.git;branch=master \
           file://0001-hacked-Makefile.patch \
"

# author	Luc Van Oostenryck <lucvoo@kernel.org>	2024-02-03 17:12:35 +0100
# committer	Luc Van Oostenryck <lucvoo@kernel.org>	2024-02-03 17:12:35 +0100
# commit	0196afe16a50c76302921b139d412e82e5be2349 (patch)
# tree		3eaeea56f33f93dda57250f5957648d5efd1fe6e
# parent	09411a7a5127516a0741eb1bd8762642fa9197ce (diff)
# parent	77b30af89aa02b98420b9cff6bdc6892647e00f1 (diff)
# download	sparse-0196afe16a50c76302921b139d412e82e5be2349.tar.gz

SRCREV = "0196afe16a50c76302921b139d412e82e5be2349"

# SRCREV = "c4706aa764f3ae68258ba60be6325a5662900362"
S = "${WORKDIR}/git"

inherit pkgconfig 

do_compile() {
    oe_runmake
}

do_install() {
    oe_runmake install DESTDIR=${D} PREFIX=${exec_prefix}
}

FILES:${PN} = "${bindir}"

RDEPENDS:${PN} = "perl"

BBCLASSEXTEND += "native nativesdk"

ERROR_QA:remove = "buildpaths"
WARN_QA:append = " buildpaths"
