# SPDX-FileCopyrightText: 2021 Robert Berger <RobertBerger@ReliableEmbeddedSystems.com>
#
# SPDX-License-Identifier: MIT

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"

SRC_URI = "https://m.seehuhn.de/programs/moon-buggy-${PV}.tar.gz"
SRC_URI[md5sum] = "bfe23ef5cfa838ac261eee34ea5322f3"
SRC_URI[sha1sum] = "7f1c5df99944acfe98eeb5f8d5ab6f28ef61ee7e"
SRC_URI[sha256sum] = "352dc16ccae4c66f1e87ab071e6a4ebeb94ff4e4f744ce1b12a769d02fe5d23f"
SRC_URI[sha384sum] = "70174964c51367ac879f1832ed7ba337997cb71ca45f8fbccb1d309aa43e1449920223e3d6c9c14f01e8af314afdee01"
SRC_URI[sha512sum] = "34da2ca8b79d4f95a762cb7142586d176fd1b58ea6f4375de424d73d6046c2dc3dbae30dddb3a78c8c2563fbfec01d9eaafe27b79cc78ffa22121ad5a2e9dc77"

SRC_URI += "file://0001-don-t-execute-moon-buggy.patch"

# NOTE: the following library dependencies are unknown, ignoring: curses
#       (this is based on recipes that have previously been built and packaged)
DEPENDS = "ncurses"

# NOTE: if this software is not capable of being built in a separate build directory
# from the source, you should replace autotools with autotools-brokensep in the
# inherit line
inherit autotools

# Specify any options you want to pass to the configure script using EXTRA_OECONF:
EXTRA_OECONF = ""

