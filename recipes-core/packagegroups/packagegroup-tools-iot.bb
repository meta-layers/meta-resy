DESCRIPTION = "Tools iot Package Group"

PACKAGE_ARCH = "${MACHINE_ARCH}"
inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "\
    ${PN}"

RDEPENDS:${PN} = "\
    mraa \
    upm \
    mosquitto \
    mosquitto-clients \
    haveged \
    ntp \
    resolvconf \
    softether \
    "

# softether
# resolvconf needed for ntp hack
# removed from above:
# rabbitmq-c
# python3-pip
# mysql-python
# python3-pyzmq

# ERROR: packagegroup-tools-iot-1.0-r0 do_package_write_ipk: An allarch packagegroup shouldn't depend on packages which are dynamically renamed (mraa to libmraa2)
# ERROR: packagegroup-tools-iot-1.0-r0 do_package_write_ipk: An allarch packagegroup shouldn't depend on packages which are dynamically renamed (mraa-dev to libmraa-dev)
# ERROR: packagegroup-tools-iot-1.0-r0 do_package_write_ipk: An allarch packagegroup shouldn't depend on packages which are dynamically renamed (mraa-dbg to libmraa-dbg)

# fix: PACKAGE_ARCH = "${MACHINE_ARCH}" before inherit packagegroup

