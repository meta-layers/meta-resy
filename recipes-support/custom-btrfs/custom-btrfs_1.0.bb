FILESEXTRAPATHS:prepend := "${THISDIR}/${MACHINE}:"
SUMMARY = "hack to create btrfs subvolumes if they are not created yet"
SECTION = "support"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# the script, which does the magic
SRC_URI = " \
    file://custom-btrfs.sh \
"

# simple backup script
SRC_URI += " \
    file://rsyncbtrfs \
    file://rsyncbtrfs-diff \
"

# systemd unit
SRC_URI += " \
    file://custom-btrfs.service \
"

# sysvinit script
SRC_URI += " \
    file://custom-btrfs \
"

# --> systemd service
inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
# disable for manual testing
# e.g. on target:
# systemctl start custom-btrfs.service
#SYSTEMD_AUTO_ENABLE = "disable"
SYSTEMD_SERVICE:custom-btrfs = "custom-btrfs.service"

# looks like this not needed
#FILES:${PN} += "${systemd_unitdir}/system/custom-btrfs.service"
# <-- systemd service

# --> sysvinit scripts
inherit update-rc.d
#INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME = "custom-btrfs"
# script has a runlevel of: 99
# starts in initlevels:     2 3 4 5
# stops  in initlevels: 0 1         6
# if we want to enable it:
# let's start it with S05
INITSCRIPT_PARAMS = "start 05 2 3 4 5 . stop 05 0 1 6 ."
# if we want to disable it:
# INITSCRIPT_PARAMS = "stop 99 0 1 6 ."
# <-- sysvinit scripts

do_install () {
        # --> install custom-btrfs.sh (common)
        # this sould work both for system-v and systemd
        install -d ${D}${sbindir}
        install -m 0755 ${WORKDIR}/custom-btrfs.sh ${D}${sbindir}
        install -m 0755 ${WORKDIR}/rsyncbtrfs ${D}${sbindir}
        install -m 0755 ${WORKDIR}/rsyncbtrfs-diff ${D}${sbindir}
        # <-- install custom-btrfs.sh (common)
        # --> systemd
        install -d ${D}${systemd_unitdir}/system
        install -m 0644 ${WORKDIR}/custom-btrfs.service ${D}${systemd_unitdir}/system
        sed -i -e 's,@SBINDIR@,${sbindir},g' ${D}${systemd_unitdir}/system/custom-btrfs.service
        # <-- systemd
        # --> sysvinit
        install -d ${D}${sysconfdir}/init.d/
        install -m 0755 ${WORKDIR}/custom-btrfs ${D}${sysconfdir}/init.d
        sed -i -e 's,@SBINDIR@,${sbindir},g' ${D}${sysconfdir}/init.d/custom-btrfs
        # <-- sysvinit
}

do_install:append () {
    # in local.conf:
    #    CONTAINER_ON_PERSISTENT = "1"

    # we have various variables defined in local.conf which are used here
    # please note, that we create here part of custom-btrfs.sh 
    # but it's parametrized via those variables in built time

    if [[ ${CONTAINER_ON_PERSISTENT} = *1* ]]
    then
    cat >> ${D}${sbindir}/custom-btrfs.sh <<EOF

dbg_echo "---------->"

# DEBUG on/off is in custom-btrfs.sh

NEEDS_REBOOT="no"

if [ \${DEBUG} = "on" ]; then
   set -x
fi

# create the (main) subvolume if it don't exist
if btrfs subvolume show ${MOUNT_FOR_CONTAINER}/${CONTAINER_SUBVOL1}; then
   dbg_echo "subvolume ${MOUNT_FOR_CONTAINER}/${CONTAINER_SUBVOL1} exists"
else
   dbg_echo "subvolume ${MOUNT_FOR_CONTAINER}/${CONTAINER_SUBVOL1} does not exist"
   # if dir exists - kill it
   if [ -d ${MOUNT_FOR_CONTAINER}/${CONTAINER_SUBVOL1} ]; then
       dbg_echo "dir ${MOUNT_FOR_CONTAINER}/${CONTAINER_SUBVOL1} exists - rm -rf it"
       rm -rf ${MOUNT_FOR_CONTAINER}/${CONTAINER_SUBVOL1}
   fi
   
   btrfs subvolume create ${MOUNT_FOR_CONTAINER}/${CONTAINER_SUBVOL1}
   if [ \${DEBUG} = "on" ]; then
      btrfs subvolume list ${MOUNT_FOR_CONTAINER}
   fi
   #NEEDS_REBOOT="yes"
fi

# create the (snapshot) subvolume if it don't exist
if btrfs subvolume show ${MOUNT_FOR_CONTAINER}/snapshot; then
   dbg_echo "subvolume ${MOUNT_FOR_CONTAINER}/snapshot exists"
else
   dbg_echo "subvolume ${MOUNT_FOR_CONTAINER}/snapshot does not exist"
   # if dir exists - kill it
   if [ -d ${MOUNT_FOR_CONTAINER}/snapshot ]; then
       dbg_echo "dir ${MOUNT_FOR_CONTAINER}/snapshot exists - rm -rf it"
       rm -rf ${MOUNT_FOR_CONTAINER}/snapshot
   fi
   
   btrfs subvolume create ${MOUNT_FOR_CONTAINER}/snapshot
   if [ \${DEBUG} = "on" ]; then
      btrfs subvolume list ${MOUNT_FOR_CONTAINER}
   fi
   #NEEDS_REBOOT="yes"
fi

if [ \${DEBUG} = "on" ]; then
   set -x
   # mount
   mount | grep btrfs

   # tree
   tree ${MOUNT_FOR_CONTAINER}

   # subvolume list
   btrfs subvolume list ${MOUNT_FOR_CONTAINER}
   set +x
fi

#if [ \${NEEDS_REBOOT} = "yes" ]; then
#   set -x
#   shutdown -r now
#   set +x
#fi


dbg_echo "<----------"

if [ \${DEBUG} = "on" ]; then
   set +x
fi

exit 0

EOF
   fi # CONTAINER_ON_PERSISTENT

}

# for sysvinit we need resolvconf as a dependency
#RDEPENDS:${PN} += "${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'resolvconf', '', d)}"
