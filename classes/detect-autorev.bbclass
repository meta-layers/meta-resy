addtask do_detectautorev before do_build after do_package
do_detectautorev[nostamp] = "1"
# package related
python do_detectautorev() {
    bb.build.exec_func("read_subpackage_metadata", d)
    for p in d.getVar("PACKAGES", True).split():
#       bb.warn("-->")
#       bb.warn("Got package %s" % p)
#       bb.warn("Got SRCREV  %s" % d.getVar("SRCREV", True))
        if bb.utils.contains('SRCREV', 'AUTOINC', True, False, d):
            bb.warn("package %s SRCREV contains AUTOINC" % p)
#            bb.warn("SRCREV_pn: %s" % d.getVar("SRCREV_pn", True))
#            bb.warn("SRCREV_pn: %s" % d, d.getVar('BUILDHISTORY_DIR_PACKAGE', True))
#       bb.warn("<--")
}
