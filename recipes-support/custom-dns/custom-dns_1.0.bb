FILESEXTRAPATHS:prepend := "${THISDIR}/${MACHINE}:"
SUMMARY = "hack to use local dns with rootfs over nfs + ips over kernel commandline"
SECTION = "support"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# the script, which does the magic
SRC_URI = " \
    file://custom-dns.sh \
"

# systemd unit
SRC_URI += " \
    file://custom-dns.service \
"

# sysvinit script
SRC_URI += " \
    file://custom-dns \
"

S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"


# --> systemd service
inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
# disable for manual testing
# e.g. on target:
# systemctl start custom-dns.service
#SYSTEMD_AUTO_ENABLE = "disable"
SYSTEMD_SERVICE:custom-dns = "custom-dns.service"

# looks like this not needed
#FILES:${PN} += "${systemd_unitdir}/system/custom-dns.service"
# <-- systemd service

# --> sysvinit scripts
inherit update-rc.d
#INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME = "custom-dns"
# script has a runlevel of: 99
# starts in initlevels:     2 3 4 5
# stops  in initlevels: 0 1         6
# if we want to enable it:
# let's start it with S05
INITSCRIPT_PARAMS = "start 05 2 3 4 5 . stop 05 0 1 6 ."
# if we want to disable it:
# INITSCRIPT_PARAMS = "stop 99 0 1 6 ."
# <-- sysvinit scripts

do_install () {
        # --> install custom-dns.sh (common)
        # this sould work both for system-v and systemd
        install -d ${D}${sbindir}
        install -m 0755 ${UNPACKDIR}/custom-dns.sh ${D}${sbindir}
        # <-- install custom-dns.sh (common)
        # --> systemd
        install -d ${D}${systemd_unitdir}/system
        install -m 0644 ${UNPACKDIR}/custom-dns.service ${D}${systemd_unitdir}/system
        sed -i -e 's,@SBINDIR@,${sbindir},g' ${D}${systemd_unitdir}/system/custom-dns.service
        # <-- systemd
        # --> sysvinit
        install -d ${D}${sysconfdir}/init.d/
        install -m 0755 ${UNPACKDIR}/custom-dns ${D}${sysconfdir}/init.d
        sed -i -e 's,@SBINDIR@,${sbindir},g' ${D}${sysconfdir}/init.d/custom-dns
        # <-- sysvinit
}

# for sysvinit we need resolvconf as a dependency
RDEPENDS:${PN} += "${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'resolvconf', '', d)}"
