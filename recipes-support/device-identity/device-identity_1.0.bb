#FILESEXTRAPATHS:prepend := "${THISDIR}/${MACHINE}:"
SUMMARY = "unique device identity"
SECTION = "support"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI += " \
    file://device-identity \
"
S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"

do_install() {
	# place in /bin
        install -d ${D}/${bindir}
	# read/execute
        install -m 0555 ${UNPACKDIR}/device-identity ${D}/${bindir}/device-identity
}
