# those should end up in the nativesdk

RDEPENDS:${PN} += " \
    nativesdk-dtc \
    nativesdk-kmod \
"
# does not work anymore like this - hardknott
#RDEPENDS:${PN} += " \
#    nativesdk-go-dep \
#    nativesdk-go \
#"

RDEPENDS:${PN} += " \
     nativesdk-go \
     nativesdk-go-runtime \
"

RDEPENDS:${PN} += " \
   nativesdk-rust \
   nativesdk-rust-llvm \
   nativesdk-cargo \
"

RDEPENDS:${PN} += " \
    nativesdk-make \
    nativesdk-cmake \
"
RDEPENDS:${PN} += " \
    nativesdk-e2fsprogs \
    nativesdk-mtd-utils \
    nativesdk-squashfs-tools \
"
RDEPENDS:${PN} += " \
    nativesdk-elfutils \
"
RDEPENDS:${PN} += " \
    nativesdk-lzop \
    nativesdk-xz \
"
RDEPENDS:${PN} += " \
    nativesdk-u-boot-tools \
"
RDEPENDS:${PN} += " \
    nativesdk-wget \
"
RDEPENDS:${PN} += " \
    nativesdk-cppcheck \
    nativesdk-sparse \
"
RDEPENDS:${PN} += " \
    nativesdk-ccze \
"



# we try to get nativesdk-agent-proxy via dynamic layers

#
#RDEPENDS:${PN} += " \
#    nativesdk-elftosb \
#    nativesdk-mxsldr \
#    nativesdk-u-boot-mkimage \
#    nativesdk-imx-usb-loader \
#    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'nativesdk-wayland', '', d)} \
#"


# nativesdk-git
# git clone https://github.com/influxdata/influxdb.git
#Cloning into 'influxdb'...
#fatal: unable to access 'https://github.com/influxdata/influxdb.git/': error setting certificate verify locations:
#  CAfile: /opt/resy/3.1/sysroots/x86_64-resysdk-linux/etc/ssl/certs/ca-certificates.crt
#  CApath: none

