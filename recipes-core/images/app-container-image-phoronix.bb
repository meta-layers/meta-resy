# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "An image containing the Phoronix Test Suite"

require recipes-core/images/app-container-image.bb
require recipes-core/images/app-container-image-phoronix-common.inc
