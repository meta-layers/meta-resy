#
# Copyright (C) 2021 Robert Berger - Reliable Embedded Systems e.U.
# SPDX-License-Identifier: MIT
#

SRCREV = "039cd8edf0bb6967068bd314dbb579530ca34ad5"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a50d0469eb02a4651c9625d696b22a69"
