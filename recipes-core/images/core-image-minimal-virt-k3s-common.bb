# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/common-container-img.inc

IMAGE_INSTALL += "\
screen \
byobu \
rsync \
"
