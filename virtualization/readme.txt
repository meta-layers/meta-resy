 systemctl list-units | grep docker
● docker.service                                                                                                loaded failed failed    Docker Application Container Engine                                                                 
● docker.socket                                                                                                 loaded failed failed    Docker Socket for the API                    

-----

root@multi-v7-ml:~# docker --version
Docker version 18.09.3-ce, build f5e591e


root@multi-v7-ml:~# docker info
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?


---

----
root@multi-v7-ml:~# dockerd 
INFO[2019-05-15T19:27:42.736443268Z] libcontainerd: started new containerd process  pid=525
INFO[2019-05-15T19:27:42.737446640Z] parsed scheme: "unix"                         module=grpc
INFO[2019-05-15T19:27:42.737675649Z] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2019-05-15T19:27:42.738385009Z] ccResolverWrapper: sending new addresses to cc: [{unix:///var/run/docker/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2019-05-15T19:27:42.738598684Z] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2019-05-15T19:27:42.739000367Z] pickfirstBalancer: HandleSubConnStateChange: 0x3c20510, CONNECTING  module=grpc
INFO[2019-05-15T19:27:43.051717812Z] starting containerd                           revision=e6b3f5632f50dbc4e9cb6288d911bf4f5e95b18e.m version=v1.2.4.m
INFO[2019-05-15T19:27:43.055511626Z] loading plugin "io.containerd.content.v1.content"...  type=io.containerd.content.v1
INFO[2019-05-15T19:27:43.057865050Z] loading plugin "io.containerd.snapshotter.v1.aufs"...  type=io.containerd.snapshotter.v1
WARN[2019-05-15T19:27:43.106197252Z] failed to load plugin io.containerd.snapshotter.v1.aufs  error="modprobe aufs failed: "modprobe: FATAL: Module aufs not found in directory /lib/modules/4.19.13-custom-ml-debug\n": exit status 1"
INFO[2019-05-15T19:27:43.106608601Z] loading plugin "io.containerd.snapshotter.v1.native"...  type=io.containerd.snapshotter.v1
INFO[2019-05-15T19:27:43.109015027Z] loading plugin "io.containerd.snapshotter.v1.overlayfs"...  type=io.containerd.snapshotter.v1
INFO[2019-05-15T19:27:43.116140303Z] loading plugin "io.containerd.snapshotter.v1.zfs"...  type=io.containerd.snapshotter.v1
WARN[2019-05-15T19:27:43.118111379Z] failed to load plugin io.containerd.snapshotter.v1.zfs  error="path /var/lib/docker/containerd/daemon/io.containerd.snapshotter.v1.zfs must be a zfs filesystem to be used with the zfs snapshotter"
INFO[2019-05-15T19:27:43.118352388Z] loading plugin "io.containerd.metadata.v1.bolt"...  type=io.containerd.metadata.v1
WARN[2019-05-15T19:27:43.119565435Z] could not use snapshotter zfs in metadata plugin  error="path /var/lib/docker/containerd/daemon/io.containerd.snapshotter.v1.zfs must be a zfs filesystem to be used with the zfs snapshotter"
WARN[2019-05-15T19:27:43.119783777Z] could not use snapshotter aufs in metadata plugin  error="modprobe aufs failed: "modprobe: FATAL: Module aufs not found in directory /lib/modules/4.19.13-custom-ml-debug\n": exit status 1"
INFO[2019-05-15T19:27:43.127844422Z] loading plugin "io.containerd.differ.v1.walking"...  type=io.containerd.differ.v1
INFO[2019-05-15T19:27:43.128186769Z] loading plugin "io.containerd.gc.v1.scheduler"...  type=io.containerd.gc.v1
INFO[2019-05-15T19:27:43.129522820Z] loading plugin "io.containerd.service.v1.containers-service"...  type=io.containerd.service.v1
INFO[2019-05-15T19:27:43.129809831Z] loading plugin "io.containerd.service.v1.content-service"...  type=io.containerd.service.v1
INFO[2019-05-15T19:27:43.130063508Z] loading plugin "io.containerd.service.v1.diff-service"...  type=io.containerd.service.v1
INFO[2019-05-15T19:27:43.130306184Z] loading plugin "io.containerd.service.v1.images-service"...  type=io.containerd.service.v1
INFO[2019-05-15T19:27:43.130555194Z] loading plugin "io.containerd.service.v1.leases-service"...  type=io.containerd.service.v1
INFO[2019-05-15T19:27:43.130848205Z] loading plugin "io.containerd.service.v1.namespaces-service"...  type=io.containerd.service.v1
INFO[2019-05-15T19:27:43.131079547Z] loading plugin "io.containerd.service.v1.snapshots-service"...  type=io.containerd.service.v1
INFO[2019-05-15T19:27:43.131494897Z] loading plugin "io.containerd.runtime.v1.linux"...  type=io.containerd.runtime.v1
INFO[2019-05-15T19:27:43.133672314Z] loading plugin "io.containerd.runtime.v2.task"...  type=io.containerd.runtime.v2
INFO[2019-05-15T19:27:43.136061073Z] loading plugin "io.containerd.monitor.v1.cgroups"...  type=io.containerd.monitor.v1
INFO[2019-05-15T19:27:43.141589620Z] loading plugin "io.containerd.service.v1.tasks-service"...  type=io.containerd.service.v1
INFO[2019-05-15T19:27:43.141963968Z] loading plugin "io.containerd.internal.v1.restart"...  type=io.containerd.internal.v1
INFO[2019-05-15T19:27:43.142621993Z] loading plugin "io.containerd.grpc.v1.containers"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.142902004Z] loading plugin "io.containerd.grpc.v1.content"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.143153014Z] loading plugin "io.containerd.grpc.v1.diff"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.143408024Z] loading plugin "io.containerd.grpc.v1.events"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.143664700Z] loading plugin "io.containerd.grpc.v1.healthcheck"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.143959045Z] loading plugin "io.containerd.grpc.v1.images"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.144188387Z] loading plugin "io.containerd.grpc.v1.leases"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.144453731Z] loading plugin "io.containerd.grpc.v1.namespaces"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.144676406Z] loading plugin "io.containerd.internal.v1.opt"...  type=io.containerd.internal.v1
INFO[2019-05-15T19:27:43.149286918Z] loading plugin "io.containerd.grpc.v1.snapshots"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.150349959Z] loading plugin "io.containerd.grpc.v1.tasks"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.151280995Z] loading plugin "io.containerd.grpc.v1.version"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.151548672Z] loading plugin "io.containerd.grpc.v1.introspection"...  type=io.containerd.grpc.v1
INFO[2019-05-15T19:27:43.155271149Z] serving...                                    address="/var/run/docker/containerd/containerd-debug.sock"
INFO[2019-05-15T19:27:43.158683948Z] serving...                                    address="/var/run/docker/containerd/containerd.sock"
INFO[2019-05-15T19:27:43.160097669Z] containerd successfully booted in 0.118584s  
INFO[2019-05-15T19:27:43.162556764Z] pickfirstBalancer: HandleSubConnStateChange: 0x3c20510, READY  module=grpc
INFO[2019-05-15T19:27:43.268103845Z] systemd-resolved is running, so using resolvconf: /run/systemd/resolve/resolv.conf 
INFO[2019-05-15T19:27:43.390877925Z] parsed scheme: "unix"                         module=grpc
INFO[2019-05-15T19:27:43.391182603Z] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2019-05-15T19:27:43.392172642Z] ccResolverWrapper: sending new addresses to cc: [{unix:///var/run/docker/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2019-05-15T19:27:43.392471320Z] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2019-05-15T19:27:43.392856335Z] pickfirstBalancer: HandleSubConnStateChange: 0x3c0e740, CONNECTING  module=grpc
INFO[2019-05-15T19:27:43.397093498Z] parsed scheme: "unix"                         module=grpc
INFO[2019-05-15T19:27:43.397755857Z] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2019-05-15T19:27:43.398147873Z] pickfirstBalancer: HandleSubConnStateChange: 0x3c0e740, READY  module=grpc
INFO[2019-05-15T19:27:43.399288917Z] ccResolverWrapper: sending new addresses to cc: [{unix:///var/run/docker/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2019-05-15T19:27:43.399525593Z] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2019-05-15T19:27:43.399901940Z] pickfirstBalancer: HandleSubConnStateChange: 0x3c0e890, CONNECTING  module=grpc
INFO[2019-05-15T19:27:43.403112398Z] pickfirstBalancer: HandleSubConnStateChange: 0x3c0e890, READY  module=grpc
ERRO[2019-05-15T19:27:43.408529274Z] Failed to built-in GetDriver graph btrfs /var/lib/docker 
ERRO[2019-05-15T19:27:43.451252926Z] 'overlay' not found as a supported filesystem on this host. Please ensure kernel is new enough and has overlay support loaded.  storage-driver=overlay2
ERRO[2019-05-15T19:27:43.497383376Z] AUFS was not found in /proc/filesystems       storage-driver=aufs
ERRO[2019-05-15T19:27:43.544276856Z] 'overlay' not found as a supported filesystem on this host. Please ensure kernel is new enough and has overlay support loaded.  storage-driver=overlay
ERRO[2019-05-15T19:27:43.544708206Z] Failed to built-in GetDriver graph devicemapper /var/lib/docker 
INFO[2019-05-15T19:27:43.799734732Z] Graph migration to content-addressability took 0.00 seconds 
WARN[2019-05-15T19:27:43.801669807Z] Your kernel does not support cgroup memory limit 
WARN[2019-05-15T19:27:43.801862814Z] Unable to find cpu cgroup in mounts          
WARN[2019-05-15T19:27:43.802036821Z] Unable to find blkio cgroup in mounts        
WARN[2019-05-15T19:27:43.802201494Z] Unable to find cpuset cgroup in mounts       
WARN[2019-05-15T19:27:43.802894854Z] mountpoint for pids not found                
INFO[2019-05-15T19:27:43.808741080Z] stopping healthcheck following graceful shutdown  module=libcontainerd
INFO[2019-05-15T19:27:43.810774826Z] stopping event stream following graceful shutdown  error="context canceled" module=libcontainerd namespace=plugins.moby
INFO[2019-05-15T19:27:43.813156918Z] pickfirstBalancer: HandleSubConnStateChange: 0x3c0e890, TRANSIENT_FAILURE  module=grpc
INFO[2019-05-15T19:27:43.813430262Z] pickfirstBalancer: HandleSubConnStateChange: 0x3c0e890, CONNECTING  module=grpc
Error starting daemon: Devices cgroup isn't mounted
root@multi-v7-ml:~# dockerd 

--------------------------

On a second attempt with a reconfigured kernel it looks a bit better:

INFO[2019-05-15T20:22:26.060849624Z] libcontainerd: started new containerd process  pid=645
INFO[2019-05-15T20:22:26.062630998Z] parsed scheme: "unix"                         module=grpc
INFO[2019-05-15T20:22:26.063465351Z] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2019-05-15T20:22:26.066597756Z] ccResolverWrapper: sending new addresses to cc: [{unix:///var/run/docker/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2019-05-15T20:22:26.067778450Z] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2019-05-15T20:22:26.069122481Z] pickfirstBalancer: HandleSubConnStateChange: 0x3d09b40, CONNECTING  module=grpc
INFO[2019-05-15T20:22:26.503228139Z] starting containerd                           revision=e6b3f5632f50dbc4e9cb6288d911bf4f5e95b18e.m version=v1.2.4.m
INFO[2019-05-15T20:22:26.507699241Z] loading plugin "io.containerd.content.v1.content"...  type=io.containerd.content.v1
INFO[2019-05-15T20:22:26.510649643Z] loading plugin "io.containerd.snapshotter.v1.aufs"...  type=io.containerd.snapshotter.v1
WARN[2019-05-15T20:22:26.570109678Z] failed to load plugin io.containerd.snapshotter.v1.aufs  error="modprobe aufs failed: "modprobe: FATAL: Module aufs not found in directory /lib/modules/4.19.13-custom-ml-debug\n": exit status 1"
INFO[2019-05-15T20:22:26.571273705Z] loading plugin "io.containerd.snapshotter.v1.native"...  type=io.containerd.snapshotter.v1
INFO[2019-05-15T20:22:26.574599114Z] loading plugin "io.containerd.snapshotter.v1.overlayfs"...  type=io.containerd.snapshotter.v1
INFO[2019-05-15T20:22:26.583065976Z] loading plugin "io.containerd.snapshotter.v1.zfs"...  type=io.containerd.snapshotter.v1
WARN[2019-05-15T20:22:26.586040711Z] failed to load plugin io.containerd.snapshotter.v1.zfs  error="path /var/lib/docker/containerd/daemon/io.containerd.snapshotter.v1.zfs must be a zfs filesystem to be used with the zfs snapshotter"
INFO[2019-05-15T20:22:26.586939732Z] loading plugin "io.containerd.metadata.v1.bolt"...  type=io.containerd.metadata.v1
WARN[2019-05-15T20:22:26.588805108Z] could not use snapshotter aufs in metadata plugin  error="modprobe aufs failed: "modprobe: FATAL: Module aufs not found in directory /lib/modules/4.19.13-custom-ml-debug\n": exit status 1"
WARN[2019-05-15T20:22:26.589603126Z] could not use snapshotter zfs in metadata plugin  error="path /var/lib/docker/containerd/daemon/io.containerd.snapshotter.v1.zfs must be a zfs filesystem to be used with the zfs snapshotter"
INFO[2019-05-15T20:22:26.599192680Z] loading plugin "io.containerd.differ.v1.walking"...  type=io.containerd.differ.v1
INFO[2019-05-15T20:22:26.599505354Z] loading plugin "io.containerd.gc.v1.scheduler"...  type=io.containerd.gc.v1
INFO[2019-05-15T20:22:26.599910364Z] loading plugin "io.containerd.service.v1.containers-service"...  type=io.containerd.service.v1
INFO[2019-05-15T20:22:26.601295062Z] loading plugin "io.containerd.service.v1.content-service"...  type=io.containerd.service.v1
INFO[2019-05-15T20:22:26.602071413Z] loading plugin "io.containerd.service.v1.diff-service"...  type=io.containerd.service.v1
INFO[2019-05-15T20:22:26.603779119Z] loading plugin "io.containerd.service.v1.images-service"...  type=io.containerd.service.v1
INFO[2019-05-15T20:22:26.604771476Z] loading plugin "io.containerd.service.v1.leases-service"...  type=io.containerd.service.v1
INFO[2019-05-15T20:22:26.605626495Z] loading plugin "io.containerd.service.v1.namespaces-service"...  type=io.containerd.service.v1
INFO[2019-05-15T20:22:26.606434180Z] loading plugin "io.containerd.service.v1.snapshots-service"...  type=io.containerd.service.v1
INFO[2019-05-15T20:22:26.607228199Z] loading plugin "io.containerd.runtime.v1.linux"...  type=io.containerd.runtime.v1
INFO[2019-05-15T20:22:26.610366604Z] loading plugin "io.containerd.runtime.v2.task"...  type=io.containerd.runtime.v2
INFO[2019-05-15T20:22:26.613702014Z] loading plugin "io.containerd.monitor.v1.cgroups"...  type=io.containerd.monitor.v1
INFO[2019-05-15T20:22:26.619537815Z] loading plugin "io.containerd.service.v1.tasks-service"...  type=io.containerd.service.v1
INFO[2019-05-15T20:22:26.620522838Z] loading plugin "io.containerd.internal.v1.restart"...  type=io.containerd.internal.v1
INFO[2019-05-15T20:22:26.621760867Z] loading plugin "io.containerd.grpc.v1.containers"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.623017562Z] loading plugin "io.containerd.grpc.v1.content"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.623871915Z] loading plugin "io.containerd.grpc.v1.diff"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.624833604Z] loading plugin "io.containerd.grpc.v1.events"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.625275281Z] loading plugin "io.containerd.grpc.v1.healthcheck"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.625537620Z] loading plugin "io.containerd.grpc.v1.images"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.625862294Z] loading plugin "io.containerd.grpc.v1.leases"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.626110300Z] loading plugin "io.containerd.grpc.v1.namespaces"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.626357972Z] loading plugin "io.containerd.internal.v1.opt"...  type=io.containerd.internal.v1
INFO[2019-05-15T20:22:26.630602737Z] loading plugin "io.containerd.grpc.v1.snapshots"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.630897077Z] loading plugin "io.containerd.grpc.v1.tasks"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.631160416Z] loading plugin "io.containerd.grpc.v1.version"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.631413422Z] loading plugin "io.containerd.grpc.v1.introspection"...  type=io.containerd.grpc.v1
INFO[2019-05-15T20:22:26.635876525Z] serving...                                    address="/var/run/docker/containerd/containerd-debug.sock"
INFO[2019-05-15T20:22:26.637774902Z] serving...                                    address="/var/run/docker/containerd/containerd.sock"
INFO[2019-05-15T20:22:26.638067242Z] containerd successfully booted in 0.146398s  
INFO[2019-05-15T20:22:26.648022804Z] pickfirstBalancer: HandleSubConnStateChange: 0x3d09b40, READY  module=grpc
INFO[2019-05-15T20:22:26.777270779Z] systemd-resolved is running, so using resolvconf: /run/systemd/resolve/resolv.conf 
INFO[2019-05-15T20:22:26.842521281Z] parsed scheme: "unix"                         module=grpc
INFO[2019-05-15T20:22:26.842777287Z] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2019-05-15T20:22:26.843534637Z] ccResolverWrapper: sending new addresses to cc: [{unix:///var/run/docker/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2019-05-15T20:22:26.843846645Z] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2019-05-15T20:22:26.844237320Z] pickfirstBalancer: HandleSubConnStateChange: 0x3d080a0, CONNECTING  module=grpc
INFO[2019-05-15T20:22:26.845586018Z] parsed scheme: "unix"                         module=grpc
INFO[2019-05-15T20:22:26.845846024Z] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2019-05-15T20:22:26.847895404Z] pickfirstBalancer: HandleSubConnStateChange: 0x3d080a0, READY  module=grpc
INFO[2019-05-15T20:22:26.849359771Z] ccResolverWrapper: sending new addresses to cc: [{unix:///var/run/docker/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2019-05-15T20:22:26.849738447Z] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2019-05-15T20:22:26.850477797Z] pickfirstBalancer: HandleSubConnStateChange: 0x401a080, CONNECTING  module=grpc
INFO[2019-05-15T20:22:26.850841806Z] blockingPicker: the picked transport is not ready, loop back to repick  module=grpc
INFO[2019-05-15T20:22:26.853068524Z] pickfirstBalancer: HandleSubConnStateChange: 0x401a080, READY  module=grpc
ERRO[2019-05-15T20:22:26.854867232Z] Failed to built-in GetDriver graph btrfs /var/lib/docker 
ERRO[2019-05-15T20:22:26.908285128Z] 'overlay2' is not supported over nfs          storage-driver=overlay2
ERRO[2019-05-15T20:22:26.959398971Z] AUFS was not found in /proc/filesystems       storage-driver=aufs
ERRO[2019-05-15T20:22:27.010958157Z] 'overlay' is not supported over nfs           storage-driver=overlay
ERRO[2019-05-15T20:22:27.011277164Z] Failed to built-in GetDriver graph devicemapper /var/lib/docker 
INFO[2019-05-15T20:22:27.263054607Z] Graph migration to content-addressability took 0.00 seconds 
WARN[2019-05-15T20:22:27.265940007Z] Your kernel does not support swap memory limit 
WARN[2019-05-15T20:22:27.267537377Z] Your kernel does not support cgroup cfs period 
WARN[2019-05-15T20:22:27.267979720Z] Your kernel does not support cgroup cfs quotas 
WARN[2019-05-15T20:22:27.268847740Z] Your kernel does not support cgroup blkio weight 
WARN[2019-05-15T20:22:27.269291750Z] Your kernel does not support cgroup blkio weight_device 
WARN[2019-05-15T20:22:27.269737427Z] Your kernel does not support cgroup blkio throttle.read_bps_device 
WARN[2019-05-15T20:22:27.270179437Z] Your kernel does not support cgroup blkio throttle.write_bps_device 
WARN[2019-05-15T20:22:27.270635115Z] Your kernel does not support cgroup blkio throttle.read_iops_device 
WARN[2019-05-15T20:22:27.271088792Z] Your kernel does not support cgroup blkio throttle.write_iops_device 
WARN[2019-05-15T20:22:27.273478846Z] mountpoint for pids not found                
INFO[2019-05-15T20:22:27.276300911Z] Loading containers: start.                   
WARN[2019-05-15T20:22:27.334042903Z] Running modprobe bridge br_netfilter failed with message: modprobe: WARNING: Module bridge not found in directory /lib/modules/4.19.13-custom-ml-debug
modprobe: WARNING: Module br_netfilter not found in directory /lib/modules/4.19.13-custom-ml-debug
, error: exit status 1 
WARN[2019-05-15T20:22:27.385459082Z] Running modprobe nf_nat failed with message: `modprobe: WARNING: Module nf_nat not found in directory /lib/modules/4.19.13-custom-ml-debug`, error: exit status 1 
WARN[2019-05-15T20:22:27.436819927Z] Running modprobe xt_conntrack failed with message: `modprobe: WARNING: Module xt_conntrack not found in directory /lib/modules/4.19.13-custom-ml-debug`, error: exit status 1 
INFO[2019-05-15T20:22:29.166511185Z] stopping event stream following graceful shutdown  error="<nil>" module=libcontainerd namespace=moby
INFO[2019-05-15T20:22:29.170420941Z] stopping healthcheck following graceful shutdown  module=libcontainerd
INFO[2019-05-15T20:22:29.170658613Z] stopping event stream following graceful shutdown  error="context canceled" module=libcontainerd namespace=plugins.moby
INFO[2019-05-15T20:22:29.174919044Z] pickfirstBalancer: HandleSubConnStateChange: 0x401a080, TRANSIENT_FAILURE  module=grpc
INFO[2019-05-15T20:22:29.175215051Z] pickfirstBalancer: HandleSubConnStateChange: 0x401a080, CONNECTING  module=grpc
Error starting daemon: Error initializing network controller: error obtaining controller instance: failed to create NAT chain DOCKER: iptables failed: iptables -t nat -N DOCKER: modprobe: FATAL: Module ip_tables not found in directory /lib/modules/4.19.13-custom-ml-debug
iptables v1.6.2: can't initialize iptables table `nat': Table does not exist (do you need to insmod?)
Perhaps iptables or your kernel needs to be upgraded.
 (exit status 3)
------------------------------



something like this:

$ mkdir -p <plnx-proj-root>/project-spec/meta-user/recipes-core/base-files
2. Create a base-files_%.bbappend file and add below content
$ vim <plnx-proj-root>/project-spec/meta-user/recipes-core/base-files/base-files_%.bbappend
  
#base-files_%.bbappend content
  
dirs755 += "/media/card"
  
do_install:append() {
    sed -i '/mmcblk0p1/s/^#//g' ${D}${sysconfdir}/fstab
}

or more flexible:

add to your image:

udev-extraconf

-----

Kernel meta data:

http://git.yoctoproject.org/cgit/cgit.cgi/linux-yocto-contrib/tree/meta/cfg/kernel-cache/cfg/fs?h=meta

-----

wget https://raw.githubusercontent.com/coreos/docker/master/contrib/check-config.sh 

and this is what we see:

root@multi-v7-ml:/# ./check-config.sh 
info: reading kernel config from /proc/config.gz ...

Generally Necessary:
- cgroup hierarchy: properly mounted [/sys/fs/cgroup]
- CONFIG_NAMESPACES: enabled
- CONFIG_NET_NS: enabled
- CONFIG_PID_NS: enabled
- CONFIG_IPC_NS: enabled
- CONFIG_UTS_NS: enabled
- CONFIG_CGROUPS: enabled
- CONFIG_CGROUP_CPUACCT: missing
- CONFIG_CGROUP_DEVICE: enabled
- CONFIG_CGROUP_FREEZER: missing
- CONFIG_CGROUP_SCHED: enabled
- CONFIG_CPUSETS: enabled
- CONFIG_MEMCG: enabled
- CONFIG_KEYS: enabled
- CONFIG_VETH: enabled
- CONFIG_BRIDGE: missing
- CONFIG_BRIDGE_NETFILTER: missing
- CONFIG_NF_NAT_IPV4: enabled (as module)
- CONFIG_IP_NF_FILTER: enabled (as module)
- CONFIG_IP_NF_TARGET_MASQUERADE: enabled (as module)
- CONFIG_NETFILTER_XT_MATCH_ADDRTYPE: enabled (as module)
- CONFIG_NETFILTER_XT_MATCH_CONNTRACK: enabled (as module)
- CONFIG_NETFILTER_XT_MATCH_IPVS: missing
- CONFIG_IP_NF_NAT: enabled (as module)
- CONFIG_NF_NAT: enabled (as module)
- CONFIG_NF_NAT_NEEDED: enabled
- CONFIG_POSIX_MQUEUE: enabled

Optional Features:
- CONFIG_USER_NS: enabled
- CONFIG_SECCOMP: enabled
- CONFIG_CGROUP_PIDS: missing
- CONFIG_MEMCG_SWAP: missing
- CONFIG_MEMCG_SWAP_ENABLED: missing
- CONFIG_BLK_CGROUP: enabled
- CONFIG_BLK_DEV_THROTTLING: missing
- CONFIG_IOSCHED_CFQ: enabled
- CONFIG_CFQ_GROUP_IOSCHED: missing
- CONFIG_CGROUP_PERF: missing
- CONFIG_CGROUP_HUGETLB: missing
- CONFIG_NET_CLS_CGROUP: enabled (as module)
- CONFIG_CGROUP_NET_PRIO: enabled
- CONFIG_CFS_BANDWIDTH: missing
- CONFIG_FAIR_GROUP_SCHED: enabled
- CONFIG_RT_GROUP_SCHED: enabled
- CONFIG_IP_VS: missing
- CONFIG_IP_VS_NFCT: missing
- CONFIG_IP_VS_RR: missing
- CONFIG_EXT4_FS: enabled
- CONFIG_EXT4_FS_POSIX_ACL: missing
- CONFIG_EXT4_FS_SECURITY: missing
    enable these ext4 configs if you are using ext3 or ext4 as backing filesystem
- Network Drivers:
  - "overlay":
    - CONFIG_VXLAN: missing
      Optional (for encrypted networks):
      - CONFIG_CRYPTO: enabled
      - CONFIG_CRYPTO_AEAD: enabled (as module)
      - CONFIG_CRYPTO_GCM: enabled (as module)
      - CONFIG_CRYPTO_SEQIV: enabled (as module)
      - CONFIG_CRYPTO_GHASH: enabled (as module)
      - CONFIG_XFRM: enabled
      - CONFIG_XFRM_USER: missing
      - CONFIG_XFRM_ALGO: enabled (as module)
      - CONFIG_INET_ESP: missing
      - CONFIG_INET_XFRM_MODE_TRANSPORT: enabled
  - "ipvlan":
    - CONFIG_IPVLAN: missing
  - "macvlan":
    - CONFIG_MACVLAN: enabled
    - CONFIG_DUMMY: missing
  - "ftp,tftp client in container":
    - CONFIG_NF_NAT_FTP: enabled (as module)
    - CONFIG_NF_CONNTRACK_FTP: enabled (as module)
    - CONFIG_NF_NAT_TFTP: enabled (as module)
    - CONFIG_NF_CONNTRACK_TFTP: enabled (as module)
- Storage Drivers:
  - "aufs":
    - CONFIG_AUFS_FS: missing
  - "btrfs":
    - CONFIG_BTRFS_FS: enabled
    - CONFIG_BTRFS_FS_POSIX_ACL: enabled
  - "devicemapper":
    - CONFIG_BLK_DEV_DM: missing
    - CONFIG_DM_THIN_PROVISIONING: missing
  - "overlay":
    - CONFIG_OVERLAY_FS: enabled
  - "zfs":
    - /dev/zfs: missing
    - zfs command: missing
    - zpool command: missing




-----


systemctl stop docker

systemctl start docker

------

before copying docker over here:

/dev/mmcblk0p2            7.2G     16.3M      6.9G   0% /media/cardp2

just docker - no containers yet:

/dev/mmcblk0p2            7.2G     16.3M      6.9G   0% /media/cardp2

root@multi-v7-ml:/var/lib# docker info
Containers: 0
 Running: 0
 Paused: 0
 Stopped: 0
Images: 0
Server Version: 18.09.3-ce  
Storage Driver: overlay2
 Backing Filesystem: btrfs  
 Supports d_type: true
 Native Overlay Diff: true  
Logging Driver: json-file   
Cgroup Driver: cgroupfs
Plugins:
 Volume: local
 Network: bridge host macvlan null overlay
 Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
Swarm: inactive
Runtimes: runc
Default Runtime: runc
Init Binary: docker-init
containerd version: e6b3f5632f50dbc4e9cb6288d911bf4f5e95b18e.m
runc version: 6635b4f0c6af3810594d2770f662f34ddc15b40d-dirty
init version: N/A (expected: )
Kernel Version: 4.19.13-custom-ml-debug
Operating System: Resy-virt (Reliable Embedded Systems Virtualization Reference Distro) 2.7 (olivegold)
OSType: linux
Architecture: armv7l
CPUs: 4
Total Memory: 974.2MiB
Name: multi-v7-ml
ID: 75P4:OFXU:QJFB:SB6B:NUMR:MGOE:EBUB:BYO7:6CBH:5CQY:ZHKR:3NPJ
Docker Root Dir: /media/cardp2/docker
Debug Mode (client): false  
Debug Mode (server): false  
Registry: https://index.docker.io/v1/
Labels:
Experimental: false
Insecure Registries:
 127.0.0.0/8
Live Restore Enabled: false 

WARNING: No cpu cfs quota support
WARNING: No cpu cfs period support
root@multi-v7-ml:/var/lib#  


after busybox pull:

/dev/mmcblk0p2            7.2G     16.7M      6.9G   0% /media/cardp2

root@multi-v7-ml:/# docker run -it --rm busybox
[  648.832616] docker0: port 1(vetha784fb6) entered blocking state
[  648.838743] docker0: port 1(vetha784fb6) entered disabled state
[  648.847043] device vetha784fb6 entered promiscuous mode
[  648.857273] IPv6: ADDRCONF(NETDEV_UP): vetha784fb6: link is not ready
[  654.144004] eth0: renamed from vethc972643
[  654.204131] IPv6: ADDRCONF(NETDEV_CHANGE): vetha784fb6: link becomes ready
[  654.212815] docker0: port 1(vetha784fb6) entered blocking state
[  654.219253] docker0: port 1(vetha784fb6) entered forwarding state
[  654.228117] IPv6: ADDRCONF(NETDEV_CHANGE): docker0: link becomes ready
/ # 



-----

It's in the image now:

./share/docker/check-config.sh
-----

scp student@192.168.42.1://home/student/docker-play/poky-app-container-python3-nmap-srv-arm-v7/local_scripts/reslocal-poky-app-container-python3-nmap-srv-arm-v7.tar .

root@multi-v7-ml:/media/cardp2/images# docker load < reslocal-poky-app-container-python3-nmap-srv-arm-v7.tar 
425cdb403109: Loading layer [==================================================>]  83.79MB/83.79MB
Loaded image: reslocal/poky-app-container-python3-nmap-srv-arm-v7:latest

df -h

from 

/dev/mmcblk0p2            7.2G     16.3M      6.9G   0% /media/cardp2

to

/dev/mmcblk0p2            7.2G    183.9M      6.8G   3% /media/cardp2

... but the .tar is also there

rm -f /media/cardp2/images/reslocal-poky-app-container-python3-nmap-srv-arm-v7.tar

sync

df -h

/dev/mmcblk0p2            7.2G    103.9M      6.8G   1% /media/cardp2


root@multi-v7-ml:/images# docker images
REPOSITORY                                            TAG                 IMAGE ID            CREATED             SIZE
reslocal/poky-app-container-nmap-arm-v7               latest              145ae1113e06        2 minutes ago       31.8MB
reslocal/poky-app-container-python3-nmap-srv-arm-v7   latest              be6c0b71979d        5 hours ago         80.3MB
busybox                                               latest              1204109f4bba        8 days ago          944kB

-----

modprobe br_netfilter

docker images

ID=$(docker run -t -i -d reslocal/poky-app-container-python3-nmap-srv-arm-v7 ash -l)

echo $ID
a58d6116613a15953e954f739555205c265346adde7aa8fde07334b028d9add1

docker attach ${ID}

-----

vi /etc/systemd/system/multi-user.target.wants/docker.service

#ExecStart=/usr/bin/dockerd -H fd://
ExecStart=/usr/bin/dockerd -H fd://  --data-root="/run/media/mmcblk0p2/docker"

------

I should load this automatically in my kernel script

br_netfilter

------

1) check kernel config 

/usr/share/docker/check-config.sh

2) create/mount btrfs

fdisk /dev/mmcblk0

mkfs.btrfs /dev/mmcblk0p2

mkdir -p /run/media/mmcblk0p2/docker

mount /dev/mmcblk0p2 /run/media/mmcblk0p2/docker

3) hack docker config

vi /etc/systemd/system/multi-user.target.wants/docker.service

#ExecStart=/usr/bin/dockerd -H fd://
ExecStart=/usr/bin/dockerd -H fd://  --data-root="/run/media/mmcblk0p2/docker"

4) start docker container

systemctl daemon-reload

systemctl stop docker

systemctl start docker

5) play with container

docker search busybox

docker pull busybox

docker run -it --rm busybox


-------------

mkdir -p /data/docker-compose

cd /data/docker-compose

wget https://raw.githubusercontent.com/vegasbrianc/docker-compose-demo/master/docker-compose.yml

docker-compose pull

docker-compose up

docker-compose down

------------

