# Copyright (C) 2022 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/core-image-minimal-base.bb

IMAGE_INSTALL += "\
                  packagegroup-tools-io \
"
