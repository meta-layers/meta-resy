# @TODO:
# add sys-v and systemd init

SUMMARY = "ZeroTier network virtualization service"

# Licenses identified:
#   COPYING -  ZeroTier BSL 1.1
#   LICENSE.txt -  ZeroTier BSL 1.1
#   debian/copyright -  ZeroTier BSL 1.1
#   ext/hiredis-0.14.1/COPYING - MIT - not included
#   ext/http-parser/LICENSE-MIT - MIT
#   ext/json/LICENSE.MIT - MIT
#   ext/libnatpmp/LICENSE - MIT
#   ext/miniupnpc/LICENSE - MIT
#   ext/redis-plus-plus-1.1.1/LICENSE - Apache License Version 2.0 - not included
#   java/src/com/zerotier/sdk/*.java - http://www.gnu.org/licenses/gpl-3.0.html - not included 
#   java/jni/ZT_jnilookup.cpp: * are available at: http://www.gnu.org/licenses/gpl-3.0.html - not included
#   java/jni/ZT_jnilookup.h: * are available at: http://www.gnu.org/licenses/gpl-3.0.html - not included
#   java/jni/com_zerotierone_sdk_Node.cpp: * are available at: http://www.gnu.org/licenses/gpl-3.0.html - not included

#
# NOTE: multiple licenses have been detected; they have been separated with &
# in the LICENSE value for now since it is a reasonable assumption that all
# of the licenses apply. If instead there is a choice between the multiple
# licenses then you should change the value to separate the licenses with |
# instead of &. If there is any doubt, check the accompanying documentation
# to determine which situation is applicable.
LICENSE = "ZeroTier-BSL-1.1 & MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=11bbae9cacaf61dd7fc10035f6f5c68e \
                    file://LICENSE.txt;md5=9a913ad4fdae889b528bc6213633b24c \
                    file://attic/historic/anode/LICENSE.txt;md5=d32239bcb673463ab874e80d47fae504 \
                    file://debian/copyright;md5=fa37ab40fd5e287272ec142be45a54e4 \
                    file://ext/hiredis-0.14.1/COPYING;md5=d84d659a35c666d23233e54503aaea51 \
                    file://ext/http-parser/LICENSE-MIT;md5=20d989143ee48a92dacde4f06bbcb59a \
                    file://ext/json/LICENSE.MIT;md5=f8a8f918f1513404c8366d7a63ab6d97 \
                    file://ext/libnatpmp/LICENSE;md5=63b8bf0fd09f4909d823a94c6e6fc06b \
                    file://ext/miniupnpc/LICENSE;md5=4a95d5317ee6dac993b7598848c72c1e "

SRC_URI = "git://github.com/zerotier/ZeroTierOne.git;protocol=https;branch=master"

# redis-plus-plus: Apache License Version 2.0 - not included, hence removed from LIC_FILES_CHKSUM
# file://ext/redis-plus-plus-1.1.1/LICENSE;md5=86d3f3a95c324c9479bd8986968f4327

# Modify these as desired
PV = "1.8.4+git${SRCPV}"
# Some 1.8.4 version:
SRCREV = "30c77cfee5c23baff48998f45dac3a37b89794cc"

S = "${WORKDIR}/git"

# NOTE: spec file indicates the license may be "ZeroTier BSL 1.1"

# install to ${D}:
#EXTRA_OEMAKE = " \
#		 DESTDIR=${D} \
#              "

# 1) install to ${D}
# 2) ZT_DEBUG=1 -> a series of traces will be emitted which 
#    detail the current state of aggregate links to each peer
EXTRA_OEMAKE = " \
                DESTDIR=${D} \
                ZT_DEBUG=1 \
                "
# -->
# fix 
# QA Issue: File /usr/sbin/zerotier-one in package zerotier-one 
# doesn't have GNU_HASH (didn't pass LDFLAGS?) [ldflags]
TARGET_CC_ARCH += "${LDFLAGS}"
# <--

do_install() {
	oe_runmake install
    	# install service file
	install -d ${D}${systemd_unitdir}/system
	install -c -m 0644 ${S}/debian/zerotier-one.service ${D}${systemd_unitdir}/system

}

inherit systemd
SYSTEMD_SERVICE_${PN} = "zerotier-one.service"

# Do not enable by default. zerotier requires manual setup anyway
# Before setting up systemctl enable zerotier-one ; systemctl start zerotier-one
SYSTEMD_AUTO_ENABLE = "disable"

# just in case we have parallel build issues with this:
#PARALLEL_MAKE = ""
