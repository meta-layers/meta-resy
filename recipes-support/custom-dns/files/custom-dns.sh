#!/bin/sh
#######################################################################################
# Explanation:
# if we boot with a rootfs over nfs or ip addresses over kernel commandline
# /lib/systemd/network/80-wired.network is not executed
# Like this we loose the local dns server
#
# Solution:
# We can pass ip addresses via the kernel command line in certain ways
#
# e.g.: 
# console=ttymxc1,115200 root=/dev/nfs ip=dhcp nfsroot=192.168.42.107:/opt/poky/imx8mm-lpddr4-evk-ml-rootfs,v3,tcp
# the kernel creates this:
# cat /proc/net/pnp
# #PROTO: DHCP
# domain res.training
# nameserver 192.168.42.254
# bootserver 192.168.42.1
#
# or:
# console=ttymxc1,115200 root=/dev/nfs ip=192.168.42.77:192.168.42.107:192.168.42.254:255.255.255.0:imx8mm-lpddr4-evk-ml.res.training:eth0:dhcp:192.168.42.254 nfsroot=192.168.42.107:/opt/poky/imx8mm-lpddr4-evk-ml-rootfs,v3,tcp
# cat /proc/net/pnp
# #MANUAL
# nameserver 192.168.42.254
# bootserver 192.168.42.107
#######################################################################################
# so we check here if /proc/net/pnp contains nameserver and if it does 
# with systemd:  we use systemd-resolve to add it for eth0
# with sysvinit: we use resolvconf to add 8.8.8.8 and the custom nameserver
#######################################################################################

# debug:
# export DEBUG=on

# no debug:
unset DEBUG

dbg_echo () {
          [[ "$DEBUG" ]] && builtin echo $@
}

export nameserver=$(cat /proc/net/pnp | grep nameserver | awk '{print $2}')

if [ -z "${nameserver}" ]; then
   # we have no namerserver entry in /proc/net/pnp
   dbg_echo "no nameserver"
   # return 0 so e.g. systemd does not think it failed
   exit 0
else
   # we do have a nameserver entry in /proc/net/pnp
   dbg_echo "nameserver: ${nameserver}"
   # sysvinit:
   if [[ -f /sbin/init && ! -L /sbin/init ]]; then
	   dbg_echo "it's /sbin/init"
	   export sysvinit_exe=/sbin/init
   fi
   if [[ -f /sbin/init.sysvinit ]]; then
	   dbg_echo "it's /sbin/init.sysvinit"
	   export sysvinit_exe=/sbin/init.sysvinit
   fi

   dbg_echo "pidof ${sysvinit_exe}: $(pidof ${sysvinit_exe})"

   if pidof ${sysvinit_exe} > /dev/null; then
      dbg_echo "it's sysvinit"
      # we just make some entry of nameservers in /etc/resolv.conf
      # interface is irrelevant at the moment
      echo "nameserver ${nameserver}
            nameserver 8.8.8.8
            search res.training" | resolvconf -a dummy.if
   fi
   # systemd:
   if pidof systemd > /dev/null; then
      dbg_echo "it's systemd"
      dbg_echo "systemd-resolve --interface eth0 --set-dns ${nameserver}"
      systemd-resolve --interface eth0 --set-dns ${nameserver}
   fi
fi
