# SPDX-FileCopyrightText: 2021 Robert Berger <RobertBerger@ReliableEmbeddedSystems.com>
#
# SPDX-License-Identifier: MIT

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d7ae45e19108e890d6501374320fc40f"

SRC_URI = "git://github.com/RobertBerger/ascii-invaders;branch=master;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
#SRCREV = "${AUTOREV}"
SRCREV = "2644be38afc06f7f7bba55034966dd66d9ad524f"

S = "${WORKDIR}/git/src"

DEPENDS = "ncurses"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = ""

