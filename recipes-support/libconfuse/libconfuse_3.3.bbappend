FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append = " file://0001-fix-for-CVE-2022-40320.patch"

# libconfuse       CVE-2022-40320    0.0      8.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-40320
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-40320
# needs-triage
#
# https://ubuntu.com/security/CVE-2022-40320
# cfg_tilde_expand in confuse.c in libConfuse 3.3 has a heap-based buffer over-read.
# https://github.com/libconfuse/libconfuse/issues/163
# 
# https://nvd.nist.gov/vuln/detail/CVE-2022-40320
# https://github.com/libconfuse/libconfuse/issues/163
#
# it was fixed after version 3.3 (which is the last release)
# I proposed a patch and tried to make upstream make a new release
# https://github.com/libconfuse/libconfuse/issues/177
# 
# in the meantime I backported the patch
# CVE_STATUS[CVE-2022-40320] is not needed, since it's picked up from the patch
# see meta/conf/cve-check-map.conf
