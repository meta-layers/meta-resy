= hack not needed anymore ! =

1) u-boot

1.1) if swupdate_saveenv_canary is not set

1.1.1) set it

1.1.2) setenv rootfs_partition /dev/mmcblk0p2

1.1.3) saveenv

2) wic

2.1) we need a custom extlinux.conf

2.1.1) in machine config:

# wic - wks
WKS_FILE = "beaglebone-yocto-custom.wks"

# wic should not update fstab
WIC_CREATE_EXTRA_ARGS = "--no-fstab-update"

... but this means that we will need to mount manually at least /mnt

2.1.2) in .wks

bootloader --append="console=ttyS0,115200" --configfile="beaglebone-yocto-custom-extlinux.conf"

2.1.3) beaglebone-yocto-custom-extlinux.conf:

default Yocto
label Yocto
   kernel /zImage
   fdtdir /
append root=${rootfs_partition} rootwait console=ttyS0,115200

Note: ${rootfs_partition} is a variable set in the u-boot environment and toggled after SW update

=================================================================================================

3) In Linux

3.1) in meta-swupdate-boards-res-master/recipes-bsp

3.1.1) mountmnt

checks what is the currently active partition and mounts the passive partition as /mnt

3.1.2) toggle-rootfs-partition

toggles rootfs partition after software update from lua script

=================================================================================================

== hack ==

in u-boot:

setenv rootfs_partition /dev/mmcblk0p2

saveenv, which creates /boot/uboot.env

in linux:

vim /boot/extlinux/extlinux.conf

append root=${rootfs_partition} rootwait console=ttyS0,115200

reboot

== hack end ==

== boot ==

=> ls mmc 0:1         
    67151   am335x-boneblack.dtb
    63701   am335x-bone.dtb
    63965   am335x-bonegreen.dtb
            extlinux/
   108996   MLO
  1252304   u-boot.img
  7928144   zImage
   131072   uboot.env

7 file(s), 1 dir(s)

== linux ==

cat /proc/cmdline
fdisk -l /dev/mmcblk0
mount
ls /boot
ls /mnt
ls /

after first time booting:

root@BeagleboneBlack:~# cat /proc/cmdline
root=/dev/mmcblk0p2 rootwait console=ttyS0,115200
root@BeagleboneBlack:~# mount
/dev/mmcblk0p2 on / type ext4 (rw,relatime)
devtmpfs on /dev type devtmpfs (rw,relatime,size=241116k,nr_inodes=60279,mode=755)
proc on /proc type proc (rw,relatime)
sysfs on /sys type sysfs (rw,relatime)
debugfs on /sys/kernel/debug type debugfs (rw,relatime)
tmpfs on /run type tmpfs (rw,nosuid,nodev,mode=755)
tmpfs on /var/volatile type tmpfs (rw,relatime)
/dev/mmcblk0p1 on /boot type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro)
/dev/mmcblk0p3 on /mnt type ext4 (rw,relatime)
devpts on /dev/pts type devpts (rw,relatime,gid=5,mode=620,ptmxmode=000)
root@BeagleboneBlack:~# ls /boot
MLO  am335x-bone.dtb  am335x-boneblack.dtb  am335x-bonegreen.dtb  extlinux  u-boot.img  uboot.env  zImage
root@BeagleboneBlack:~# ls /mnt
lost+found
root@BeagleboneBlack:~# ls /
bin  boot  dev  etc  home  lib  lost+found  media  mnt  proc  run  sbin  sys  tmp  usr  var  www
root@BeagleboneBlack:~# 


------




-----

how to get rid of uuid in /boot/extlinux/extlinux.conf?

cat extlinux.conf 

default Yocto
label Yocto
   kernel /zImage
   fdtdir /
append root=/dev/mmcblk0p2 rootwait console=ttyS0,115200


bitbake u-boot -e | grep ^UBOOT_EXTLINUX
UBOOT_EXTLINUX_CONFIG="/workdir/build/beaglebone-yocto-swupdate-master/tmp/work/beaglebone_yocto_custom-poky-linux-gnueabi/u-boot/1_2022.07-r0/build/extlinux.conf"
UBOOT_EXTLINUX_CONF_NAME="extlinux.conf"
UBOOT_EXTLINUX_CONSOLE="console=\${console},\${baudrate}"
UBOOT_EXTLINUX_FDT=""
UBOOT_EXTLINUX_FDTDIR="../"
UBOOT_EXTLINUX_INSTALL_DIR="/boot/extlinux"
UBOOT_EXTLINUX_KERNEL_ARGS="rootwait rw"
UBOOT_EXTLINUX_KERNEL_IMAGE="../zImage"
UBOOT_EXTLINUX_LABELS="linux"
UBOOT_EXTLINUX_MENU_DESCRIPTION:linux="Poky (Yocto Project Reference Distro)"
UBOOT_EXTLINUX_SYMLINK="extlinux.conf-beaglebone-yocto-custom-r0"
UBOOT_EXTLINUX_VARS="CONSOLE MENU_DESCRIPTION ROOT KERNEL_IMAGE FDTDIR FDT KERNEL_ARGS INITRD"

u-boot RPROVIDES u-boot-extlinux
