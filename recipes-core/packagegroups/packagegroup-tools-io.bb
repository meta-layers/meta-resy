DESCRIPTION = "Tools i/o Package Group"

PACKAGE_ARCH = "${MACHINE_ARCH}"
inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "\
    packagegroup-tools-io"

# i2c tools

RDEPENDS:packagegroup-tools-io = "\
i2c-tools \
"

# not sure, but spidev-test and spitools
# might be the same thing,
# one built from kernel sources
# the other otherwise

RDEPENDS:packagegroup-tools-io +="\
spidev-test \
spitools \
"

# root@m100pfsevp:~# iotop
# Could not run iotop as some of the requirements are not met:
# - Linux >= 2.6.20 with
#  - I/O accounting support (CONFIG_TASKSTATS, CONFIG_TASK_DELAY_ACCT, CONFIG_TASK_IO_ACCOUNTING)

RDEPENDS:packagegroup-tools-io +="\
iotop \
"

# libuio
# A user-space library for interfacing with generic uio devices.
# ERROR: packagegroup-tools-io-1.0-r0 do_package_write_ipk: An allarch packagegroup shouldn't depend on packages which are dynamically renamed (libuio to libuio0)
# fix: PACKAGE_ARCH = "${MACHINE_ARCH}" before inherit packagegroup
RDEPENDS:packagegroup-tools-io +="\
libuio \
"

# devmem2
# a simple program to read/write from/to any location in memory.

RDEPENDS:packagegroup-tools-io +="\
devmem2 \
"

# evtest
# Input device event monitor and query tool

RDEPENDS:packagegroup-tools-io +="\
evtest \
"
# libsoc
# a C library to interface with common peripherals found in
# System on Chips (SoC) through generic Linux Kernel interfaces.
# ERROR: packagegroup-tools-io-1.0-r0 do_package_write_ipk: An allarch packagegroup shouldn't depend on packages which are dynamically renamed (libsoc to libsoc2)
# fix: PACKAGE_ARCH = "${MACHINE_ARCH}" before inherit packagegroup
RDEPENDS:packagegroup-tools-io +="\
libsoc \
"

# Zlog
# a pure C logging library

RDEPENDS:packagegroup-tools-io +="\
zlog \
"

# libgpiod
# library and tools for interacting with the linux GPIO character device
# ERROR: packagegroup-tools-io-1.0-r0 do_package_write_ipk: An allarch packagegroup shouldn't depend on packages which are dynamically renamed (libgpiod to libgpiod2)
# fix: PACKAGE_ARCH = "${MACHINE_ARCH}" before inherit packagegroup
RDEPENDS:packagegroup-tools-io +="\
libgpiod \
libgpiod-tools \
"

# industrial io
# ERROR: packagegroup-tools-io-1.0-r0 do_package_write_ipk: An allarch packagegroup shouldn't depend on packages which are dynamically renamed (libiio to libiio0)
# fix: PACKAGE_ARCH = "${MACHINE_ARCH}" before inherit packagegroup
RDEPENDS:packagegroup-tools-io +="\
libiio \
lsiio \
iio-event-monitor \
iio-generic-buffer \
"

# lmsensors
# ERROR: packagegroup-tools-io-1.0-r0 do_package_write_ipk: An allarch packagegroup shouldn't depend on packages which are dynamically renamed (lmsensors-libsensors-dev to libsensors-dev)
# fix: PACKAGE_ARCH = "${MACHINE_ARCH}" before inherit packagegroup
RDEPENDS:packagegroup-tools-io +="\
lmsensors-sensors \
lmsensors-sensorsdetect \
lmsensors-sensorsconfconvert \
lmsensors-pwmconfig \
lmsensors-libsensors \
"
