# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "A java container image"

require recipes-core/images/app-container-image.bb
require dynamic-layers/meta-java/recipes-core/images/app-container-image-java-common.inc
