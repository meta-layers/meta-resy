# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/common-container-img.inc

# pick your preferred docker from e.g. local.conf:

# docker-moby/runc-opencontainers seems to run in 256M RAM

# PREFERRED_PROVIDER_virtual/docker = "docker-moby" <--
# PREFERRED_PROVIDER_virtual/docker = "docker-ce"

# PREFERRED_PROVIDER_virtual/runc = "runc-opencontainers" <--

IMAGE_INSTALL += "\
                  docker \
                 "

IMAGE_INSTALL += "\
                 python3-docker-compose \
"
