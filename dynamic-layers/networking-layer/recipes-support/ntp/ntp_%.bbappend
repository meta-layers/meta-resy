FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
# Note that you might need the custom-dns package together with this
# e.g. if you run with a rootfs over nfs or ip addresses over kernel command line

# daemon.err ntpd[712]: frequency file /var/lib/ntp/drift.TEMP: Permission denied
SRC_URI:append = " ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'file://volatiles.ntp.conf', 'file://volatiles.20_ntp', d)}"

do_install:append() {
        # add some ntp servers after this line:
        #     server time.server.example.com
        #     this (/etc/ntp.conf) is valid for sys-v and systemd
        sed -i '/^# server time.server.example.com/a #server pfsense.res.training\nserver 0.rhel.pool.ntp.org iburst\nserver 1.rhel.pool.ntp.org iburst\nserver 2.rhel.pool.ntp.org iburst\nserver 3.rhel.pool.ntp.org iburst\nserver 0.pool.ntp.org\nserver 1.pool.ntp.org\nserver 2.pool.ntp.org\nserver 3.pool.ntp.org\n' ${D}${sysconfdir}/ntp.conf
}
