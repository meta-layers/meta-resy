#
# Writes image-manifest information to rootfs 
# default location: /etc/image-manifest 
#
# Copyright (C) 2024 Reliable Embedded Systems e.U.
# Author: Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>
#
# Licensed under the MIT license, see COPYING.MIT for details
#
# Usage:    add inherit image-manifestinfo to your image recipe
#        or add INHERIT += "image-manifestinfo" to your config file
#

# Default location of the image-manifest in the image.
IMAGE_MANIFEST_FILE ??= "${sysconfdir}/image-manifest"

# modified from rootfs-postcommands.bbclass:python write_image_manifest ()
# which writes the image_manifest to the deploy dir
python write_image_manifest_to_rootfs () {
    from oe.rootfs import image_list_installed_packages
    from oe.utils import format_pkg_list

    pkgs = image_list_installed_packages(d)
    with open(d.expand('${IMAGE_ROOTFS}${IMAGE_MANIFEST_FILE}'), 'w') as image_manifest:
        image_manifest.write(format_pkg_list(pkgs, "ver"))
}

# run it - some time late deferred until after parsing
ROOTFS_POSTUNINSTALL_COMMAND:append = " write_image_manifest_to_rootfs ;"
