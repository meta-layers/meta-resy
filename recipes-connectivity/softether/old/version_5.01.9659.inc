#
# Copyright (C) 2019 Robert Berger - Reliable Embedded Systems e.U.
# SPDX-License-Identifier: MIT
#

# you could enforce this version if you add this e.g. to your local.conf
# PREFERRED_VERSION:pn-softether = "5.01.9659"
SRCREV = "a25fea0809eaf3e2397584588cab40511f69037d"
LIC_FILES_CHKSUM = "file://LICENSE;md5=80bcfc91c01fb22ea037ed8afa5bbe75"
