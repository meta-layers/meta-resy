# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   debian/copyright
#
# NOTE: multiple licenses have been detected; they have been separated with &
# in the LICENSE value for now since it is a reasonable assumption that all
# of the licenses apply. If instead there is a choice between the multiple
# licenses then you should change the value to separate the licenses with |
# instead of &. If there is any doubt, check the accompanying documentation
# to determine which situation is applicable.
LICENSE = "GPL-3.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=f27defe1e96c2e1ecd4e0c9be8967949 \
                    file://debian/copyright;md5=6fd5e63a300f9021643c1b5ab8188255"

SRC_URI = "git://github.com/dustinkirkland/byobu;protocol=https;branch=master"

# Modify these as desired
SRCREV = "bfb7a763d0ea458b310962a4fd7538420f613b6b"
PV = "5.134+git${SRCPV}"

S = "${WORKDIR}/git"

# NOTE: if this software is not capable of being built in a separate build directory
# from the source, you should replace autotools with autotools-brokensep in the
# inherit line
inherit autotools

# Specify any options you want to pass to the configure script using EXTRA_OECONF:
EXTRA_OECONF = ""

FILES:${PN} += " \
    ${datadir}/dbus-1/services/us.kirkland.terminals.byobu.service \
"

# for now let's add the dependencies, 
# but looks like it's tests, 
# so we could get rid of it
# ERROR: byobu-5.121+gitAUTOINC+bfb7a763d0-r0 do_package_qa: QA Issue: /usr/share/byobu/tests/byobu-time-notifications contained in package byobu requires /bin/bash, but no providers found in RDEPENDS:byobu? [file-rdeps]
# ERROR: byobu-5.121+gitAUTOINC+bfb7a763d0-r0 do_package_qa: QA Issue: /usr/lib/byobu/include/notify_osd contained in package byobu requires /usr/bin/perl, but no providers found in RDEPENDS:byobu? [file-rdeps]
RDEPENDS:${PN} = "bash perl glibc-utils"
RDEPENDS:${PN} = "bash perl"

# remove this dependency when building with musl
REPENDS:${PN}:remove:libc-musl = "glibc-utils"

do_install:append() {
# replace default tmux with screen
        sed -i 's/BYOBU_BACKEND="tmux"/BYOBU_BACKEND="screen"/' ${D}${sysconfdir}/byobu/backend
}

