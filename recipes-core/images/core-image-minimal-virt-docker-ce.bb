# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL += "\
                  dropbear \
                  e2fsprogs-mke2fs \
                  bash \
                  btrfs-tools \
                  udev-extraconf \
                  python3 \
                  glibc-gconv-utf-16 \
                  softether \
                  ccze \
"


#IMAGE_INSTALL += " \
#                 docker-ce \
#                 docker-ce-contrib \
#"

IMAGE_INSTALL += "\
                  docker \
                 "


IMAGE_INSTALL += "\
                 python3 \
                 python3-docker-compose \
"


#IMAGE_INSTALL += "\
#                  util-linux \
#                  podman \
#                  podman-compose \
#                  conmon \
#"

# udev-extraconf for automount

IMAGE_INSTALL += "\
                  packagegroup-tools-io \
                  packagegroup-tools-cmdline \
                  packagegroup-tools-benchmark \
                  packagegroup-tools-top \
                  packagegroup-tools-rt \
"

IMAGE_INSTALL += "\
		 mosquitto-clients \
		 ntp \
                 "
# enable stuff from MACHINE/DISTRO_FEATURES
# I added this in local.conf, since it's small
#CORE_IMAGE_EXTRA_INSTALL += "packagegroup-base-extended"
