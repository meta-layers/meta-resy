# Copyright (C) 2024 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

inherit image-mode

require recipes-core/images/core-image-minimal.bb

IMAGE_MODE = "production"
