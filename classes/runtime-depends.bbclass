addtask printrdeps before do_build after do_package do_packagedata
do_printrdeps[nostamp] = "1"
# package related
python do_printrdeps() {
    bb.build.exec_func("read_subpackage_metadata", d)
    for p in d.getVar("PACKAGES", True).split():
        bb.warn("--> package")
        bb.warn("Got package %s" % p)
        bb.warn("Got RDEPENDS %s" % d.getVar("RDEPENDS:" + p, True))
        bb.warn("Got RRECOMMENDS %s" % d.getVar("RRECOMMENDS:" + p, True))
        bb.warn("Got RSUGGESTS %s" % d.getVar("RSUGGESTS:" + p, True))
        bb.warn("Got RCONFLICTS %s" % d.getVar("RCONFLICTS:" + p, True))
        bb.warn("Got RPROVIDES %s" % d.getVar("RPROVIDES:" + p, True))
        bb.warn("Got RREPLACES %s" % d.getVar("RREPLACES:" + p, True))
        bb.warn("Got FILES %s" % d.getVar("FILES:" + p, True))
        bb.warn("Got pkg_preinst %s" % d.getVar("pkg_preinst:" + p, True))
        bb.warn("Got pkg_postinst %s" % d.getVar("pkg_postinst:" + p, True))
        bb.warn("Got pkg_prerm %s" % d.getVar("pkg_prerm:" + p, True))
        bb.warn("Got pkg_postrm %s" % d.getVar("pkg_postrm:" + p, True))
        bb.warn("Got ALLOW_EMPTY %s" % d.getVar("ALLOW_EMPTY:" + p, True))
        bb.warn("Got PRIVATE_LIBS %s" % d.getVar("PRIVATE_LIBS:" + p, True))
#        --> Those are recipe and not package related
#        bb.warn("Got DEPENDS %s" % d.getVar("DEPENDS:" + p, True))
#        bb.warn("Got PROVIDES %s" % d.getVar("PROVIDES:" + p, True))
#        <-- Those are recipe and not package related
        bb.warn("<-- package")
}

# recipe related
addtask printbdeps before do_build after do_package do_packagedata
do_printbdeps[nostamp] = "1"
python do_printbdeps() {
    bb.warn("--> recipe")
    bb.warn("Got DEPENDS %s" % d.getVar("DEPENDS", True))
    bb.warn("Got PROVIDES %s" % d.getVar("PROVIDES", True))
    bb.warn("<-- recipe")
}

# busybox

