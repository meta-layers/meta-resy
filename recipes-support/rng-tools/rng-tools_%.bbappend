FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# shell script which should work for init-v and systemd
SRC_URI += "\
           file://rng-tools-helper.sh \
"

# at the moment we don't support systemd yet
SRC_URI:append = " ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', '', 'file://0001-exclude-jitter-source-from-local-CPUs-if-caam-hardwa.patch;patchdir=${WORKDIR}', d)}"

do_install() {
        # shell script, which adds to EXTRA_ARGS if necessary
        install -d ${D}${sbindir}
        install -m 0755 ${UNPACKDIR}/rng-tools-helper.sh ${D}${sbindir}
}
