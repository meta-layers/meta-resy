# my gitea runner runs with user/group 70 which causes:
# ERROR: sysvinit-3.04-r0 do_package_qa: QA Issue: sysvinit: /sbin/shutdown.sysvinit is owned by gid 70, which is the same as the user running bitbake. This may be due to host contamination [host-user-contaminated]
# ERROR: sysvinit-3.04-r0 do_package_qa: QA Issue: sysvinit: /sbin/halt.sysvinit is owned by gid 70, which is the same as the user running bitbake. This may be due to host contamination [host-user-contaminated]
# ERROR: sysvinit-3.04-r0 do_package_qa: Fatal QA errors were found, failing task.
# ERROR: Logfile of failure stored in: /workspace/rber/gitea-yocto-build/workdir/build/multi-v7-ml-debug-training-master/tmp/work/armv7at2hf-neon-resy-linux-gnueabi/sysvinit/3.04/temp/log.do_package_qa.1148751
# NOTE: recipe sysvinit-3.04-r0: task do_package_qa: Failed

ERROR_QA:remove = "host-user-contaminated"
WARN_QA:append = " host-user-contaminated"

