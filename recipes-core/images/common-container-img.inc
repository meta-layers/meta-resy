# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

# this ends up in the "native" rootfs

require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL += "\
                  dropbear \
                  htop \
                  vim \
                  git \
                  curl \
                  tree \
                  bash \
                  btrfs-tools \
                  ccze \
                  python3 \
"

IMAGE_INSTALL += "\
                  e2fsprogs-mke2fs \
                  btrfs-tools \
"

IMAGE_INSTALL += "\
                  udev-extraconf \
"

IMAGE_INSTALL += "\
                 custom-dns \
                 "

IMAGE_INSTALL += "\
                 ntp \
                 "

CORE_IMAGE_EXTRA_INSTALL += "packagegroup-base-extended"

# @@@ TODO: review this:

# in local.conf:
# PREFERRED_PROVIDER_virtual/runc = "runc-opencontainers"

CORE_IMAGE_EXTRA_INSTALL += " containerd-opencontainers runc-opencontainers"
CORE_IMAGE_EXTRA_INSTALL += " skopeo oci-image-tools"

CORE_IMAGE_EXTRA_INSTALL += " util-linux iptables"
