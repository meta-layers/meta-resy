# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL += " ${@bb.utils.contains("DISTRO_FEATURES", "soft-watchdog", "watchdog", "" ,d)}"

IMAGE_INSTALL += "\
                  dropbear \
                 "

IMAGE_INSTALL += "\
                  git \
                  curl \
                 "

IMAGE_INSTALL += "\
                  bash \
                 "

IMAGE_INSTALL += "\
                  e2fsprogs-mke2fs \
                  btrfs-tools \
                 "

# udev-extraconf for automount
IMAGE_INSTALL += "\
                  udev-extraconf \
                 "

IMAGE_INSTALL += "\
                  stress-ng \
                 "

IMAGE_INSTALL += "\
                  custom-dns \
                 "

IMAGE_INSTALL += "\
                  ntp \
                 "

#IMAGE_INSTALL += "\
#                  telegraf \
#                 "

IMAGE_INSTALL += "\
                  packagegroup-tools-cmdline \
                  packagegroup-tools-top \
                 "

IMAGE_INSTALL += "${@bb.utils.contains('DISTRO_FEATURES','systemd','libcgroup','',d)}"

#packagegroup-tools-benchmark
#packagegroup-tools-io
#packagegroup-tools-rt

# add cyclictest and others
IMAGE_INSTALL += "\
                  packagegroup-tools-rt \
                 "

CORE_IMAGE_EXTRA_INSTALL += "\
                             packagegroup-base-extended \
                            "
