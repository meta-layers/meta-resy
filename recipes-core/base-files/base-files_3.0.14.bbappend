FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
#FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://dot.profile \
            file://dot.vimrc \
            file://my-motd \
           "

do_install:prepend () {
        cp ${UNPACKDIR}/dot.profile ${UNPACKDIR}/share/dot.profile
}

do_install:append () {
        # 755 permissions not necessary for .vimrc?
        install -m 0644 ${UNPACKDIR}/dot.vimrc ${D}${sysconfdir}/skel/.vimrc
        install -m 0644 ${UNPACKDIR}/my-motd ${D}${sysconfdir}/motd
}

# I put the comment out of the below snippet otherwise it throws the QA Issue ;)
# QA Issue: pkg_postinst in base-files recipe contains ${D}, 
# it should be replaced by $D instead [expanded-d]

pkg_postinst:${PN}:append () {
  install -m 0600 $D${sysconfdir}/skel/.profile $D${ROOT_HOME}/.profile
  install -m 0600 $D${sysconfdir}/skel/.vimrc $D${ROOT_HOME}/.vimrc
}

